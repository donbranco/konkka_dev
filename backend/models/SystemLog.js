const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collections
const SystemLog = new Schema({
  type: {
    type: Number
  },
  data:
  {
    uid: Number,
    pid: Number,
    action: String,
    message: String
  },
  timestamp: {
    type: Date
  }
},{
  collection: 'logs'
});

module.exports = mongoose.model('SystemLog', SystemLog); 