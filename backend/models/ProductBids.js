const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collections
const ProductBids = new Schema({
  pid: String,
  auction_ends: String,
  bids: [{
    uid: String,
    bid_amount: Number,
    timestamp: String,
    auction_ends: String,
    proxy: Boolean
  }],
  proxy: {
    uid: String,
    max_bid: Number,
    increment: Number,
  }
},{
  collection: 'bids'
});

module.exports = mongoose.model('ProductBids', ProductBids);
