const express = require('express');

const DrupalRouter = express.Router();

DrupalRouter.route('/update').post(function(req, res) {
  req.io.emit('BID_BEEN_PLACED', false)
  const pid = req.body.pid
  try {
    console.log('[UPDATE OBSERVABLE --- RECEIVE_BID_IS_SET] ', pid)
    req.io.emit('RECEIVE_BID_IS_SET', true)
    res.sendStatus(200)
  } catch(e) {
    req.io.emit('RECEIVE_BID_IS_SET', true)
    res.sendStatus(500)
  }
});

module.exports = DrupalRouter;