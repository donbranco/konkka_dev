const express = require("express");
const ProductBidsRouter = express.Router();
// const util = require("util");

const ProductBids = require("../models/ProductBids");

ProductBidsRouter.route("/").get(function(req, res) {
  ProductBids.find(function(err, productbids) {
    if (err) {
      console.log(err);
    } else {
      res.json(productbids);
    }
  }).sort({ bids: -1 });
});

ProductBidsRouter.route("/add").post(function(req, res) {
  const productbids = new ProductBids(req.body);

  ProductBids.find({ pid: productbids.pid }, (err, productbid) => {
    if (productbid.length === 0) {
      productbids
        .save()
        .then(() => { res.status(200).send("[ENDPOINT] Product bid added succesfully") })
        .catch(err => { res.status(400).send("unable to save to database: ", err) })
    } else {
      ProductBids.updateOne(
        { pid: productbids.pid },
        { $addToSet: { bids: productbids.bids } }
      )
        .then(() => { res.json("[ENDPOINT] Updated new product with new bid succesfully") })
        .catch(err => { res.status(400).send("unable to save to database: ", err) })
    }
  }).limit(1);
});

ProductBidsRouter.route("/:pid").get((req, res) => {
  ProductBids.find({ pid: req.params.pid }, (err, productbids) => {
    if (err) {
      console.log(err);
    } else {
      res.json(productbids);
    }
  });
});

ProductBidsRouter.route("/:pid/sort/highest").get((req, res) => {
  ProductBids.aggregate([
    { $match: { pid: req.params.pid } },
    { $unwind: { path: "$bids", includeArrayIndex: "index" } },
    { $sort: { "bids.bid_amount": -1 } },
    // { $limit: 10 }
  ]).then(result => {
    res.json(result);
  });
});

// * delete entire product product from mongodb
ProductBidsRouter.route("/delete/:pid/").post((req, res) => {
  console.log("[BEFORE DELETE req] --- " + req[1] + " ----- " + req.params.pid);
  console.log("[BEFORE DELETE res] --- " + res[1]);
  ProductBids.findOneAndDelete({ pid: req.params.pid }, function(
    err,
    productbid
  ) {
    if (err) throw err;
    console.log("[DELETED]" + err);
    console.log("[DELETED]" + productbid);
  });
  res.json("[SUCCESFULLY DELETED PRODUCT]");
});

// * remove bid from product bid list
ProductBidsRouter.route("/remove/bid/:pid/").post((req, res) => {
  console.log("body:--- " + req.body.data.time_stamp);
  const ts = req.body.data.time_stamp;
  const pid = req.params.pid;

  ProductBids.findOneAndUpdate(
    { pid: pid },
    { $pull: { bids: { timestamp: ts } } },
    { new: true },
    function(err, doc) {
      console.log("[ERROR] --- " + err);
      console.log("[DOC] --- " + JSON.stringify(doc));
    }
  );
  res.json("[SUCCESFULlY REMOVED BID ]");
});

ProductBidsRouter.route("/user/:uid").get((req, res) => {
  ProductBids.find({ uid: req.params.uid }, (err, productbids) => {
    if (err) {
      console.log(err);
    } else {
      res.json(productbids);
    }
  }).sort({ _id: -1 });
});



// * ------ proxy bids ------ *//
// add proxy bid
ProductBidsRouter.route("/add/proxy").post((req, res) => {
  const proxybid = new ProductBids(req.body);

  ProductBids.find({ pid: proxybid.pid }, (err, proxy) => {
    // if there are no proxybids, save first one
    console.log('route prox: ', proxy)
    if (proxy.length === 0) {
      proxybid.save()
      .then(productbid => {
        console.log('[PROXY BID SAVED] ', productbid)
        res.json("[ENDPOINT] proxy bid added succesfully");
        res.sendStatus(200)
      })
      .catch(err => {
        console.log('[ERROR ADDING PROXY BID] ', err)
        res.status(400).send("unable to save to database");
      });
    } else {
      ProductBids.updateOne(
        { pid: proxybid.pid },
        { $set: { proxy: proxybid.proxy } },
        { $addToSet: { bids: proxybid.bids, proxy: proxybid.proxy } }
      )
      .then(proxy => {
        console.log('[PROXY BID UPDATED -------- ] ', proxy)
        res.json("[ENDPOINT] Updated proxy bid succesfully");
      })
      .catch(err => {
        console.log('[ERROR - PROXY BID UPDATED] ', err)
        res.status(400).send("unable to save to database");
      });
    }
  }).limit(1);
});

// add proxy bid
ProductBidsRouter.route("/set/proxy").post((req, res) => {
  const proxy = req.body;
  const filter = { pid: proxy.pid };
  const update = { 
    proxy: {
      uid: proxy.uid,
      max_bid: proxy.max_bid,
      increment: proxy.increment
    } 
  };

  ProductBids.findOneAndUpdate(filter, update, false, false)
  .then(() => res.status(200).send("[ENDPOINT] Proxy updated succesfully"))
  .catch(e => console.log(e))
});

module.exports = ProductBidsRouter;