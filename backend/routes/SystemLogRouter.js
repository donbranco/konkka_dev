/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */

const express = require('express');
// const app = express();
const SystemLogRouter = express.Router();
// var ObjectID = require('mongodb').objectID;

const SystemLog = require('../models/SystemLog');

SystemLogRouter.route('/').get(function (req, res) {
  console.log('cool')
  SystemLog.find(function (err, log) {
    if(err) {
      console.log(err);
    }
    else {
      res.json(log);
    }
  }).sort({ _id: -1 });
});





SystemLogRouter.route('/add').post(function(req, res) {
  req.io.emit('BID_BEEN_PLACED', false)
  res.json('[ENDPOINT] New log: ')

  const logs = new SystemLog(req.body);

  logs.save()
  .then(response => {
    req.io.emit('UPDATE_LOG', true)
    res.json('[ENDPOINT] New log: ', response)
  })
  .catch(err => res.status(err.status).json(err))
})






SystemLogRouter.route('/type/:typeid').get(function (req, res) {
  SystemLog.find({type: req.params.typeid}, (err, logs) => {
    if(err) {
      console.log(err);
    } else {
      res.json(logs);
    }
  }).sort({ _id: -1 });
});

module.exports = SystemLogRouter;