// TODO: ------------- ROUTE Settings for DEV-SERVER && local ------------------

const ROUTES = require('../src/routes/routes')
const api = require('./api')

const NODE_ADDRESS = ROUTES.NODE_ROOT_HTTPS
const DRUPAL_ADDRESS = ROUTES.DRUPAL_API_ROOT

// ! ------------- Settings for DEV-SERVER ------------------

// // const tls = require('tls')
// const fs = require('fs')
// const express = require("express")
// const mongoose = require("mongoose")
// const bodyParser = require("body-parser")
// const cors = require("cors")
// const axios = require("axios")
// const moment = require("moment")
// const util = require("util")

// const options = {
// 	key: fs.readFileSync('/etc/letsencrypt/live/konkurssi.donbran.co/privkey.pem'),
// 	cert: fs.readFileSync('/etc/letsencrypt/live/konkurssi.donbran.co/cert.pem'),
// 	ca: fs.readFileSync('/etc/letsencrypt/live/konkurssi.donbran.co/chain.pem'),
// 	rejectUnauthorized: false,
// 	secure: true,
// 	reconnect: true,
// }

// const app = express()
// // const httpServer = require("http").createServer(app)
// const httpsServer = require("https").createServer(options, app)

// const ioOptions = {}
// const io = require("socket.io")(httpsServer, ioOptions)
// // io.listen(httpsServer)
// io.origins((origin, cb) => {
// 	const allowedOrigin = /\.donbran\.co(:[0-9]+)?$/
// 	// const allowedOrigin = /\dev\.konkurssi\.donbran\.co(:[0-9]+)?$/
// 	const isAllowed = allowedOrigin.test(origin)
// 	if (isAllowed) {
// 		cb(null, true)
// 	} else {
// 		cb('Not allowed', false)
// 	}
// })

// const config = require("./DB")
// const ProductBidsRouter = require("./routes/ProductBidsRouter")
// const SystemLogRouter = require("./routes/SystemLogRouter")

// // console.log(util.inspect(httpsServer, false, null, true /* enable colors */))
// // console.log(util.inspect(httpsServer, false, null, true /* enable colors */))

// mongoose.connect(config.DB).then(
// 	() => { console.log("Database is connected") },
// 	err => { console.log("Cannot connect to db" + err) }
// )

// app.use(cors({
// 	credentials: true,
// 	origin: [
// 		/\.donbran\.co$/,
// 		/\.donbran\.co:[0-9]+$/,
// 	],
// }))

// app.use(bodyParser.urlencoded({ extended: true }))
// app.use(bodyParser.json())

// app.use("/productbids", ProductBidsRouter)
// app.use("/logs", SystemLogRouter)

// // app.get('/product/:id', function (req, res, next) {
// //   res.json({msg: 'This is CORS-enabled for a whitelisted domain.'})
// //   console.log(util.inspect('no fucking cors errorrrrrrr', false, null, true /* enable colors */))
// // })

// // * ids to check auctions
// var auctions_nearly_ending = []

// ! ------------- End Settings DEV-SERVER ------------------

// ? ------------- Settings LOCAL-SERVER ------------------

const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const axios = require('axios')
// const util = require('util')

const config = require('./DB')
const ProductBidsRouter = require('./routes/ProductBidsRouter')
const SystemLogRouter = require('./routes/SystemLogRouter')
const DrupalRoutes = require('./routes/DrupalRouter')

const moment = require('moment')


mongoose.connect(config.DB).then(
	() => {
		console.log('Database is connected')
	},
	(err) => {
		console.log('Cannot connect to db' + err)
	}
)

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


app.use(function(req, res, next) {
	req.io = io;
	next();
});

app.use('/productbids', ProductBidsRouter)
app.use('/logs', SystemLogRouter)
app.use('/product-data', DrupalRoutes);

var auctions_nearly_ending = []

// ? ------------- Settings LOCAL-SERVER ------------------

// * io events
io.on('connection', (client) => {
	function completingBid(new_bid) {
		// test if this fixes the bid list after updating bids
		setTimeout(() => {
			console.log('update front end', new_bid)
			// update client bidlist with new bid
			client.emit('RECEIVE_BID', new_bid)
			client.emit('UPDATE_REACT', true)
			client.broadcast.emit('RECEIVE_BID', new_bid)
			client.broadcast.emit('UPDATE_REACT', true)
		}, 1200);
	}

	function bidMessage(type, content) {
		console.log('UPDATE REACT WITH MESSAGE')
		const data = {type: type, content: content}
		client.emit('BID_MESSAGE', data)
		client.broadcast.emit('BID_MESSAGE', data)
	}

	// when a bid is received
	client.on('SEND_BID', function(data) {
		const { pid, bid_amount } = data
		const bid_ts = moment().valueOf() / 1000
		let current_uid = data.uid
		let bid_value = bid_amount
		let proxy_state = false

		// setting the state of react to prepare for reload 
		client.emit('UPDATE_REACT', false)
		client.broadcast.emit('UPDATE_REACT', false)

		console.log('[SERVER] Received bid from client')
		console.log('uid: ' + current_uid + ' product: ' + pid + ' Bid: ' + bid_amount)

		// check if pid exists, else get data from drupal
		axios.get(NODE_ADDRESS + '/productbids/' + pid + '/sort/highest')
		.then(async res => {
			if (res.data.length > 0 ) {
				const proxy = res.data[0].proxy
				const node = res.data[0].bids
				const saved_highest_bid = parseInt(node.bid_amount)
				const auction_ends = moment(parseInt(node.auction_ends))
				
				console.log('[SERVER] -- product exists in mongodb')

				// compare current time & bid with data in mongodb
				if (bid_ts < auction_ends) {
					if (parseInt(bid_amount) >= saved_highest_bid) {
						placeBidToNode( pid, current_uid, bid_value, bid_ts, auction_ends, proxy_state )
						.then(res => 	{
							completingBid(res)
							placeBidToDrupal()
							bidMessage("success", "Bid is approved and being confirmed")
						})

						// place proxy bid if exists
						if(typeof proxy !== 'undefined') {
							console.log('[UPDATE NEW BID WITH A PROXY BID]')
							// update bid with proxy bid if max_bid is not exceeded
							proxy_state = true
							current_uid = proxy.uid
							bid_value = bid_value + proxy.increment

							if(proxy.max_bid > bid_value) {
								placeBidToNode( pid, current_uid, bid_value, bid_ts, auction_ends, proxy_state )
								.then(res => { 
									completingBid(res)
									bidMessage("error", "There is a proxy bid that has a higher maximum bid")
								})
								.catch(e => console.log(e))
							} else {
								console.log('[MAX_BID PROXY BID WAS TOO SMALL]')
							}
						} 
					} else {
						console.log('[SERVER] -- bid is too low')
					}
				} else {
					console.log('[SERVER] --  auction has ended')
				}
				//  end node check
			} else {
				// get data from drupal
				axios.post(DRUPAL_ADDRESS + '/products/get', {product_ids: [ pid.toString() ]})
				.then(async res => {
					console.log('[SERVER] -- get product data from DRUPAL')
					const data_drupal = JSON.parse(res.data.products[data.pid.toString()])
					const auction_ends = parseInt(data_drupal.ending_time)
					
					// place bid to drupal
					const proxy_state = false
					placeBidToNode( pid, current_uid, bid_value, bid_ts, auction_ends, proxy_state )
					.then(res => { 
						const type = "success"
						const content = "Bid is approved and being confirmed"
						bidMessage(type, content)
						completingBid(res)
					})
					.catch(e => console.log(e))

					axios.get(NODE_ADDRESS + '/productbids/' + pid)
					.then(res => 
						api.Bids.sendNewBidToDrupal(res.data[0])
						.then(() => console.log('[New Bid has successfully been added to drupal]'))
						.catch(e => console.error(e))
					)
					.catch(e => console.error(e))
				})
				.catch(e => console.error(e))
			}
		})
		.catch(e => console.error(e))
	})

	// * add proxy
	client.on('ADD_PROXY', async function(proxy) {
		const { pid, increment, current_bid } = proxy
		// auction_ends
		const res_node = await axios.get(NODE_ADDRESS + '/productbids/' + pid + '/sort/highest').catch(e => console.error(e))
		const node = res_node.data[0]
		const bid_ts = moment().valueOf() / 1000
		
		// let max_bid = proxy.max_bid
		
		console.log('proxy 1: ')

		// check if there is existing data in node
		if(typeof node !== 'undefined' && typeof node.bids !== 'undefined') {
			console.log('proxy 2 ', node)
			// const { auction_ends, bid_amount } = node.bids

			// check whether the auction is active && proxy bid is greater than current highest bid + increment
			if(bid_ts < proxy.auction_ends && proxy.max_bid > node.bids.bid_amount + increment) {
				console.log('proxy 3')

				// check if there is an existing proxy bid
				if(typeof node.proxy !== 'undefined') {
					console.log('proxy 4')

					// is the new proxy bid the same user as the existing, if so update the proxy wihtout adding a bid
					if(proxy.uid === node.bids.uid || proxy.uid === node.proxy.uid) {
						console.log('proxy 5')

						// set proxy bid of current highest bidder
						
						setProxyBid( pid, proxy.uid, proxy.max_bid, increment )
						bidMessage("success", "You just placed a proxy bid: " + proxy.max_bid)

					} else {
						console.log('proxy 6')
						// is new proxy bid greater than current proxy
						if(proxy.max_bid > node.proxy.max_bid + increment) {
							console.log('proxy 7')
							console.log('[REPLACE CURRENT PROXY WITH NEW]')

							console.log('setting proxy uid: ', proxy.uid)

							// update exisiting proxy with new proxy user
							setProxyBid( pid, proxy.uid, proxy.max_bid, increment )

							// place previous highest proxy as a bid
							console.log('placing prev max bid uid: ', node.proxy.uid)
							await placeBidToNode( pid, node.proxy.uid, node.proxy.max_bid, bid_ts, proxy.auction_ends, true )

							// place new proxy bid + increment on top
							console.log('placing new bid uid: ', proxy.uid)
							const res = await placeBidToNode( pid, proxy.uid, node.proxy.max_bid + increment, bid_ts, proxy.auction_ends, true )
							bidMessage("success", "New highest proxy bid is set and auto bid has been placed")
							completingBid(res)

							// new proxy bid is smaller than existing. Thus, place it as a bid and add a proxy bid
						} else if(proxy.max_bid < node.proxy.max_bid - increment) {
							console.log('proxy 8')
							console.log('new max bid is smaller than previous')

							// there is a higher existing proxy bid - place a normal new bid
							await placeBidToNode( pid, proxy.uid, proxy.max_bid, bid_ts, proxy.auction_ends, false )

							// place an updated a higher proxy bid
							let place_proxy_bid = proxy.max_bid + increment
							const res = await placeBidToNode( pid, node.proxy.uid, place_proxy_bid, bid_ts, proxy.auction_ends, true )
							bidMessage("warning", "Bid has been acceptend, but a proxy bid, just bid over your bid")
							completingBid(res)
								
							// awkward state where the highest proxy will have to place a higher bid than is set.
							// thus, last proxy bid will be ignored and current highestproxy will be placed
						} else {
							console.log('proxy 9')
							console.log('both proxies are at an awkward state')

							// place the current existing highest proxy bid
							const res = await placeBidToNode( pid, node.proxy.uid, node.proxy.max_bid, bid_ts, proxy.auction_ends, true )
							bidMessage("error", "There is a higher proxy bid that will reach it limits with the increment. Highest proxy bid will be placed")
							completingBid(res)
							
						}
					}
					// there are no proxy bids placed yet
					// check if there are normal bids & place first proxy bid
				} else {
					console.log('proxy 10')
					// set first proxy bid that is placed
					setProxyBid( pid, proxy.uid, proxy.max_bid, increment )

					// also place proxy bid if there is an existing bid
					// that is not the same user as the proxy
					if(proxy.uid !== node.bids.uid) {
						console.log('proxy 11')
						// add bid with current highest + increment
						const bid_value = current_bid + increment
						const res = await placeBidToNode( pid, proxy.uid, bid_value, bid_ts, proxy.auction_ends, true )
						bidMessage("success", "Proxy bid has been set and bid has been placed")
						completingBid(res)

					} else {
						console.log('proxy 12')
						bidMessage("success", "Proxy bid has been updated")
					}
				}
				// when the proxy bid is too small 
				// or whem the auction has ended -- handle errors
			} else {
				console.log('proxy 13')
				if(bid_ts > proxy.auction_ends) console.log('auction has ended ')
				if(proxy.max_bid < node.bids.bid_amount ) console.log('proxy was too small')
				// const message = 'Placed proxy bid is lower than the current maximum proxy bid of another user'
				// client.emit('PROXY_TOO_LOW', { node.bids.bid_amount, increment, message })
			}
			// set first proxy and bid if there is any bids or a proxy set
		} else {
			console.log('proxy 14')
			// sendBidWithProxy( pid, proxy.uid, bid_value, bid_ts, auction_ends, proxy_state, max_bid, increment )
			console.log(pid)
			console.log(proxy.uid)
			console.log(current_bid)
			console.log(bid_ts)
			console.log(proxy.auction_ends)
			console.log(proxy.max_bid)
			console.log(increment)
			console.log(proxy)
			const res = await sendBidWithProxy( pid, proxy.uid, current_bid, bid_ts, proxy.auction_ends, false, proxy.max_bid, increment )
			completingBid(res)
			bidMessage("success", "First proxy bid has been placed")
		}
	})

	// * remove bid from bidlist
	client.on('REMOVE_BID', function(bid) {
		const { ts, pid } = bid
		const body = { data: { time_stamp: ts } }
		const params = { params: { time_stamp: ts } }
		axios.post(`${NODE_ADDRESS}/productbids/remove/bid/${pid}`, body, params)
		.then(() => {
			console.log('[BID_IS_REMOVED] --- ' + pid)
			client.broadcast.emit('BID_IS_REMOVED', pid)
			client.emit('BID_IS_REMOVED', pid)
		})
	})
})

// * ------ helper functions ------ * //

// send a bid to node / mongodb
async function placeBidToNode( pid, current_uid, bid_value, bid_ts, auction_ends, proxy_state ) {


	console.log('place bid to node : ', pid, current_uid, bid_value, bid_ts, auction_ends, proxy_state )
	// check if the auction time needsd to eb updated to 1min
	const updated_time = await checkAuctionTime(bid_ts, auction_ends)
	const auction_ending = updated_time ? updated_time : auction_ends.valueOf()
	
	const bid_format = {
		pid: pid,
		auction_ends: auction_ending,
		bids: {
			uid: current_uid,
			bid_amount: bid_value,
			timestamp: bid_ts,
			auction_ends: auction_ending,
			proxy: proxy_state
		},
	}

	const node = await axios.post(NODE_ADDRESS + '/productbids/add', bid_format).catch(e => console.error(e))
	if(node.status === 200) return bid_format
	return false
}

// send bid to drupal
async function placeBidToDrupal(pid) {
	console.log('[SENDING ALL BIDS TO DRUPAL]')
	// send all the bids from product_id to drupal
	api.Bids.getProductData(pid)
	.then(res => {
		api.Bids.sendNewBidToDrupal(res.data[0])
		.then(result => { 
			if(result.data.status === 'teppoWINNIPEG') return true 
		})
		.catch(e => console.log(e))
	})
	.catch(e => console.log(e))
	return false
}

// set proxy bid
function setProxyBid( pid, current_uid, max_bid, increment ) {
	const proxy = {
		pid: pid,
		uid: current_uid,
    max_bid: max_bid,
    increment: increment,
	}
	console.log('[SET PROXY BID] ', proxy)

	api.Bids.setProxyBid(proxy)
	.then(() => { return proxy })
	.catch(e => console.log(e))
}

async function sendBidWithProxy( pid, current_uid, bid_value, bid_ts, auction_ends, proxy_state, max_bid, increment ) {
	// check if the auction time needsd to eb updated to 1min
	const updated_time = await checkAuctionTime(bid_ts, auction_ends)
	const auction_ending = updated_time ? updated_time : auction_ends.valueOf()
	
	const bid_format = {
		pid: pid,
		auction_ends: auction_ending,
		bids: {
			uid: current_uid,
			bid_amount: bid_value,
			timestamp: bid_ts,
			auction_ends: auction_ending,
			proxy: proxy_state
		},
		proxy: {
			uid: current_uid,
   		max_bid: max_bid,
    	increment: increment,
		}
	}

	api.Bids.sendProxyBid(bid_format)
	.catch(e => console.log(e))

	return bid_format
}

// check auction time whether or not it is less than 1min
function checkAuctionTime(ts, auction_ends) {
		const add_1_minute = moment(ts).add(1, 'm').valueOf()
		const is_less_than_1_minute = add_1_minute > auction_ends * 1000 ? true : false
		return is_less_than_1_minute ? updateAuctionTime(auction_ends) : false
}

// update auction time to 1 minute
function updateAuctionTime(old_time_stamp) {
	const ms = moment(moment(), 'ss').diff(moment(old_time_stamp * 1000, 'ss'))
	const remaining_seconds = moment.duration(ms).format('ss')
	const updated_time = moment().add(1, 'm').add(-remaining_seconds, 's')

	console.log('[UPDATE ACTION TIME TO: ]' + updated_time)
	return moment(updated_time).valueOf() / 1000
}

function checkIfProductIsTracked(pid) {
	if (auctions_nearly_ending.length > 0) {
		return auctions_nearly_ending.some((existing_pid) => {
			if (parseInt(existing_pid) === parseInt(pid)) {
				console.log('[CHECK_EXISTS] --- ' + pid)
				return true
			} else {
				return false
			}
		})
	}
}

// * check auction time and add pid to be tracked
function trackProduct(product) {
	const less_than_30m = moment(product.auction_ends * 1000) < moment().add(30, 'm') ? true : false
	const current = moment(product.auction_ends * 1000) > moment() ? true : false

	if(less_than_30m && current) auctions_nearly_ending.push(JSON.parse(product.pid))
}

// * removes ended auctions from endingAuctions[]
function clearEndingAuctions(pid) {
	console.log('[REMOVE_FROM_ARRAY] --- ' + pid)
	auctions_nearly_ending = auctions_nearly_ending.filter((id) => id !== pid)
}

function removeAuctionFromDB(pid) {
	api.Bids.deleteAuctionFromMongo(pid)
}

async function sendEndedAuctionToDrupal(pid) {
	console.log('[SEND_TO_DRUPAL] --- ' + pid)

	// normally, do not set the force flag. normally only active auctions can be ended, but force overrides that.
	// in return values ended == 1 is success, ended == 2 that the auction wasn't active and was not updated.
	// which can be overridden with force.

	const bid_data = api.Bids.listBidsByHighest(pid)
	const { auction_ends, bids } = bid_data

	const body = {
    "pid": pid,
    "auction_ends": auction_ends,
    "force": 1,
    "bids": bids
	}
	// /RESTful/end-auction
	api.Bids.completeAuction(body)
	.then(res => {
		// bid is successfully accepted in drupal
		if(res.data ===  1) {
			clearEndingAuctions(pid)
			removeAuctionFromDB(pid)
		}
		// bid is not accepted in drupal -- something failed
		if(res.data ===  2) console.log('sending completed bid to drupal failed')
	})
	.catch(e => console.log(e))

}

// *  get auctions that are ending in < 10min, checks every 5 min
// ! its every 10seconds at the moment
setInterval(function() {
	axios.get(NODE_ADDRESS + '/productbids/')
	.then(res => {
		const list_of_products = res.data

		// check if there are any auctions on the node side that are ending soon
		if (list_of_products.length > 0) {
			list_of_products.map(product => {
				const does_product_exist = checkIfProductIsTracked(product.pid) === true ? true : false
				if (!does_product_exist) return trackProduct(product)
				return null
			})
		}
	})
	.catch(e => console.error(e) )
}, 10000)

// * checks every 5 seconds if an auction has ended by comparing ts in nearEndingAuction with current
// ! note it is every 2 seconds at the moment
setInterval(function() {
	if (auctions_nearly_ending.length > 0) {
		auctions_nearly_ending.map((pid) => {
			axios.get(NODE_ADDRESS + '/productbids/' + pid + '/sort/highest').then((res) => {
				const product = res.data[0]
				const upper_time_limit = new Date(product.auction_ends * 1000) < new Date()

				if (upper_time_limit) sendEndedAuctionToDrupal(product)
			})
			.catch(e => console.error(e))
			return null
		})
		.catch(e => console.error(e))
	}
}, 2000)

// TODO: DEV-SERVER
// const port = 8000
// httpsServer.listen(port, '0.0.0.0', function() {
// 	console.log('Server running on port:  8000')
// })

// ? DEV-LOCAL
const port = 8000
server.listen(port, function() {
	console.log('Server running on port: ', port)
})




// ************************************************************** //
// ****************** Save for now as a backup ******************//
// ************************************************************** //


// // place proxy bid 
// async function placeProxyBid({ pid, current_uid, bid_amount, bid_ts, auction_ends, proxy }) {
// 	// check if the auction time needsd to eb updated to 1min
// 	const updated_time = await checkAuctionTime(bid_ts, auction_ends)
// 	const auction_ending = updated_time ? updated_time : auction_ends.valueOf()
	
// 	const bid_format = {
// 		pid: pid,
// 		auction_ends: auction_ending,
// 		bids: {
// 			uid: current_uid,
// 			bid_amount: bid_amount,
// 			timestamp: bid_ts,
// 			auction_ends: auction_ending,
// 			proxy: false
// 		},
// 		proxy: {
// 			uid: current_uid,
//    		max_bid: proxy.max_bid,
//     	increment: proxy.increment,
// 		}
// 	} 
// }



// // ! pass increment as an argument
	// async function sendBidWithProxy({ pid, uid = null, current_uid, max_bid, bid_amount, auction_ends, ts, increment }) {
	// 	// check if time needs to be updated
	// 	let send_proxy_data
	// 	const match_uid = uid === current_uid

	// 	// define auction ending time that is set
	// 	const updated_time = checkAuctionTime(ts, auction_ends)
	// 	const auction_ending = updated_time ? updated_time : auction_ends

	// 	// time when bid is placed
	// 	const bid_ts = parseInt(ts / 1000).toString()
	// 	const placing_new_bid = parseInt(bid_amount) + increment

	// 	if(match_uid) {
	// 		// data format for mongodb
	// 		console.log('[UID MATCHES]')
	// 		send_proxy_data = {
	// 			proxy: {
	// 				uid: current_uid,
	// 				max_bid: max_bid,
	// 				increment: increment,
	// 			}
	// 		}
	// 	} else {
	// 		// data format for mongodb
	// 		send_proxy_data = {
	// 			pid: pid,
	// 			auction_ends: auction_ending,
	// 			bids: {
	// 				uid: current_uid,	
	// 				bid_amount: placing_new_bid,
	// 				timestamp: bid_ts,
	// 				auction_ends: auction_ending,
	// 			proxy: true
	// 			},
	// 			proxy: {
	// 				uid: current_uid,
	// 				max_bid: max_bid,
	// 				increment: increment,
	// 			}
	// 		}
	// 	}

	// 	console.log('[PROXY BID IS SEND] ', send_proxy_data)

	// 	// set proxy to mongodb
	// 	axios.post(NODE_ADDRESS + '/productbids/add/proxy', send_proxy_data)
	// 	.then(() => {
	// 		if(match_uid){
	// 			console.log('not updating react')
	// 			return
	// 		}
	// 		sendProxyBidToDrupal(pid)
	// 		console.log('gay update react')
	// 	})
	// 	.catch(e => console.error(e))

	// 	return send_proxy_data
	// }

	// function sendProxyBidToDrupal(pid) {
	// 	axios.get(NODE_ADDRESS + '/productbids/' + pid)
	// 	.then(res => {
	// 		console.log('[SENDING NEW BIDS TO DRUPAL FROM PROXY]')
	// 		api.Bids.sendNewBidToDrupal(res.data[0])
	// 		.then(res => { console.log('[New Bid has successfully been added to drupal]') })
	// 		.catch(e => console.error(e))
	// 	})
	// 	.catch(e => console.error(e))
	// }






// async function placeBidToNode({ pid, uid, bid_amount, bid_ts, auction_ends, proxy_state, proxy, max_bid }) {
// let bid_format
// 	// is the bid placed in the last minute ?
// 	const updated_time = await checkAuctionTime(bid_ts, auction_ends)
// 	const auction_ending = updated_time ? updated_time : auction_ends.valueOf()

// 	if(proxy_state && uid !== proxy.uid && max_bid > proxy.max_bid) {
// 		const max = max_bid > proxy.max_bid ? max_bid : proxy.max_bid
// 		bid_format = {
// 			pid: pid,
// 			auction_ends: auction_ending,
// 			bids: {
// 				uid: uid,
// 				bid_amount: bid_amount,
// 				timestamp: bid_ts,
// 				auction_ends: auction_ending,
// 				proxy: proxy_state
// 			},
// 			proxy: {
// 				uid: uid,
// 				max_bid: max,
// 				increment: proxy.increment,
// 			}
// 		} 
// 		// update current users max_bid
// 	} else if(uid === proxy.uid) {
// 		bid_format= {
// 			proxy: {
// 				uid: uid,
// 				max_bid: max_bid,
// 				increment: proxy.increment,
// 			}
// 		}
// 	} else {
// 		bid_format = {
// 			pid: pid,
// 			auction_ends: auction_ending,
// 			bids: {
// 				uid: uid,
// 				bid_amount: bid_amount,
// 				timestamp: bid_ts,
// 				auction_ends: auction_ending,
// 				proxy: proxy_state
// 			}
// 		}
// 	}

// 	// add new bid to mongodb
// 	const node = await axios.post(NODE_ADDRESS + '/productbids/add', bid_format).catch(e => console.error(e))

// 	// check if request is successful
// 	if(node.status === 200){
// 		return bid_format
// 	} else {
// 		// error 
// 		return false
// 	}
// }

// async function placeBidToDrupal(pid) {
// 	const res = await axios.get(NODE_ADDRESS + '/productbids/' + pid)
// 	console.log('[SENDING ALL BIDS TO DRUPAL]')

// 	// send all the bids from product_id to drupal
// 	const res_drupal = await api.Bids.sendNewBidToDrupal(res.data[0])

// 	// if success refresh product data on front end
// 	if(res_drupal.data.status === 'teppoWINNIPEG') return true

// 	// log some error
// 	return false
// }

// async function placeProxyBid({ pid, uid, bid_ts, max_bid, proxy, updatedHighestBid }) {
// 	// const newProxyBid = await getLowestProxyBid({ proxy, uid, updatedHighestBid })
// 	const { increment } = proxy
// 	let bid_amount
// 	if(uid !== proxy.uid) {
// 		// ? what to do when it is equal
// 		if(!max_bid && updatedHighestBid) bid_amount = updatedHighestBid + increment
// 		if(max_bid > proxy.max_bid) bid_amount = proxy.max_bid + increment
// 		if(max_bid < proxy.max_bid) bid_amount = max_bid + increment
// 	} else {
// 		bid_amount = max_bid
// 	}

// 	const proxy_state = true
// 	const node = await placeBidToNode({ pid, uid, bid_amount, max_bid, proxy, bid_ts, proxy_state })

// 	if(node) return node
// 	return false
// }

// async function checkProxyBid(pid) {
// 	const res = await api.Bids.getProductData(pid)
// 	const state = typeof res.data[0].proxy !== 'undefined' ? res.data[0].proxy : false
// 	console.log('check proxy: ', state)
// 	return state
// }

// async function getLowestProxyBid({ proxy, uid, updatedHighestBid }) {
// 	// filter out all max_bids that are too small
// 	// TODO: set increment when database is cleared and data is coming from drupal
// 	const increment = 99
// 	const valid_proxies = await proxy.filter(p => {
// 			if(p.max_bid > updatedHighestBid + increment && p.uid !== uid) return p
// 	});

// 	console.log('new arr: ', valid_proxies)
// 	if(typeof valid_proxies.length !== 'indefined' && valid_proxies.length > 0){
// 		const lowestProxyBid = valid_proxies.reduce((min, p) => p.max_bid < min ? p.max_bid : min, proxy[0]);
// 		console.log('looking for the lowest, maximum proxy bid', lowestProxyBid);
// 		return lowestProxyBid
// 	} else {
// 		console.log('[EXCEEDING PROXY LIMITS]')
// 		return false
// 	}
// }


// ************************************************************** //
// ****************** Save for now as a backup ******************//
// ************************************************************** //
