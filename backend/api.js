// import axios from 'axios';
// import routes from '../src/routes/routes';

const axios = require('axios')
const routes= require('../src/routes/routes')

const NODE_ROOT = routes.NODE_ROOT_HTTPS
const DRUPAL_API_ROOT = routes.DRUPAL_API_ROOT

// SETTING BIDS, PROXY TO NODE && DRUPAL AND GET PRODUCT DATA
const Bids = {
  getProductData: pid => {
    return axios.get(`${NODE_ROOT}/productbids/${pid}`)
  },
  listBids: async (pid) => {
    return await axios.get(NODE_ROOT, `/productbids/` + pid)
  },
  listBidsByHighest: async (pid) => {
    return await axios.get(NODE_ROOT, `/productbids/` + pid + `/sort/highest`)
  },
  sendNewBidToDrupal: body => {
    return axios.post(`${DRUPAL_API_ROOT}/update-bids`, body)
  },
  sendCompletedAuctionToDrupal: body => {
    return axios.post(`${DRUPAL_API_ROOT}/end-auction`, body)
  },
  setProxyBid: body => {
    return axios.post(`${NODE_ROOT}/productbids/set/proxy`, body)
  },
  sendProxyBid: body => {
    return axios.post(`${NODE_ROOT}/productbids/add/proxy`, body)
  },
  completeAuction: body => {
    return axios.post(`${DRUPAL_API_ROOT}/end-auction`, body)
  },
  deleteAuctionFromMongo: pid => {
    return axios.get(`${NODE_ROOT}/delete/` + pid)
  }
}

// ALL LOG REQUEST THAT ARE MADE
const Log = {
  getAllLogs: async () => {
    return await axios.post(`${NODE_ROOT}/logs`)
  },
  getPostByType: async (type) => {
    return await axios.post(`${NODE_ROOT}/logs/type/${type}`)
  },
  postNewLog: async (body) => {
    return await axios.post(`${NODE_ROOT}/logs/add`, body)
  },
}

module.exports = {
  Bids,
  Log
}
