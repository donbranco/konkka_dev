import React, { Component } from 'react'
import { CardText } from 'reactstrap'
import moment from 'moment'

import { Trans } from '@lingui/macro'

export default class CardCounter extends Component {
  constructor(props) {
    super(props)

    this.state = {
      time: this.props.timeLeft,
      timer: undefined,
      timeFormat: undefined
		}
	}
	// timer interval
	interval = false

	// init timer
  componentDidMount = () => this.setCountDown()
	
	// stop counter --> stopping memory leak
	componentWillUnmount = () => clearInterval(this.interval)

	// set counter data
  setCountDown = () => {
		// calc remaining time diff between now and ending time of the product
		let remainingTime = moment.duration(moment(this.state.time * 1000).diff(moment()))

		if (remainingTime > 0) {
			this.runCountDown({ remainingTime })
		} else {
			this.setState({ timer: <Trans>Auction has ended</Trans> })
		}
	}

	// start counter
	runCountDown = ({ remainingTime }) => {
    const _this = this
		
		this.interval = setInterval(() => {
			if (remainingTime > 0) {
				remainingTime = moment.duration(remainingTime - 1000, 'milliseconds')
				let timer
				let days = Math.floor(remainingTime.as('days'))
				let hours = Math.floor(remainingTime.as('hours') - days * 24)
				let minutes = Math.floor(remainingTime.as('minutes') - (days * 24 * 60 + hours * 60))
				let seconds = Math.floor(remainingTime.as('seconds') - (days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60))

				// set counter format
				if(days > 1|| hours > 1) {
					// timer = {` ${days} <Trans>d</Trans> ${hours} <Trans>h</Trans>`}
					timer = <Trans id="{days}d {hours}h" values={{ days: days, hours: hours }} />
				} else {
					timer = <Trans id="{minutes}m {seconds}s" values={{ minutes: minutes, seconds: seconds }} />
				}

				// set counter
				_this.setState({ timer })
				
			} else {
				const errorMessage = <Trans>Auction has ended</Trans>
				_this.setState({ timer: errorMessage })
			}
		}, 1000)
	}

  render() {
    return <CardText className="card__timer">{this.state.timer}</CardText>
  }
}
