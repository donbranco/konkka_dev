import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Card, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'
import io from 'socket.io-client'

import { Trans } from '@lingui/macro'

import routes from '../../routes/routes'

import CardCounter from './CardCounter'

class card extends Component {
	constructor(props) {
		super(props)
		this.socket = io(routes.NODE_ROOT_HTTPS)

		this.state = {
			time: this.props.timeLeft,
			price: this.props.price
		}

		// add socket
		this.socket.on('RECEIVE_BID', (data) => {
			// update card on the home page if ID matches
			console.log('RECEIVE_BID --- card : ', data)
			if(this.props.pid === data.pid) {
				// update price end ending time
				if(this.state.time === data.auction_ends) return this.setState({ price: parseInt(data.bids.bid_amount) })
				this.setState({ price: parseInt(data.bids.bid_amount), time: toString(data.auction_ends) })
			}
    })
	}

	render() {
		const { clicked, imageUrl, title, subtitle, city, any_price_accepted } = this.props
		const { time, price } = this.state
		const card_image = { backgroundImage: `url(${imageUrl})` }
		const _price = parseFloat(price).toFixed(2).toString().replace(".", ",")
		
		return (
			<Card className="Card konkka-card" onClick={clicked}>
				{ any_price_accepted ? <div className={`filter__image-hintarajaa`}><p><Trans>Ei hintarajaa</Trans></p></div> : null }
				<div className="card__image" alt="product card image" style={card_image} />
				<CardBody>
					<div className="card-top">
						<CardTitle>{title}</CardTitle>
						<CardTitle className='product-price'>{_price}</CardTitle>
					</div>
					<CardSubtitle>{subtitle}</CardSubtitle>
						<div className="card-bottom">
							<CardText className="location">{city}</CardText>
							<CardCounter timeLeft={time} />
						</div>
				</CardBody>
			</Card>
		)
	}
}

export default inject('productStore')(observer(card))
