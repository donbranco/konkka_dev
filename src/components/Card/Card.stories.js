import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import Card from './Card';

const stories = storiesOf('Card', module);

stories.addDecorator(withKnobs);

export const card = {
  title: 'Card title',
  subtitle: 'Subtitle',
  description: 'Description',
  price: 1000,
  timeLeft: '1 hour 25 min 10 sec',
};

stories
  .add('default', () => <Card price={card.price} timeLeft={card.timeLeft} title={text('Title', card.title)} subtitle={text('Subtitle', card.subtitle)} description={text('Description', card.description)} />)