import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

// language trans
import { Trans } from '@lingui/macro'

const ListPagination = (props) => {
	let delta = 2,
		range = [],
		rangeWithDots = [],
		l,
		nrOfPages = props.totalPagesCount,
		currentPage = props.currentPage + 1,
		backbutton,
		forwardbutton,
		newRange;

	range.push(1);

	if (nrOfPages <= 1) {
		return range;
	}

	for (let i = currentPage - delta; i <= currentPage + delta; i++) {
		if (i < nrOfPages && i > 1) {
			range.push(i);
		}
	}
	range.push(nrOfPages);

	for (let i of range) {
		if (l) {
			if (i - l === 2) {
				rangeWithDots.push(l + 1);
			} else if (i - l !== 1) {
				const dots = '...';
				rangeWithDots.push(dots);
			}
		}
		rangeWithDots.push(i);
		l = i;
	}
	newRange = rangeWithDots;

	// disable back button

	if (currentPage !== 1) {
		backbutton = (
			<PaginationLink
				className="pagination-arrow"
				previous
				onClick={() => {
					props.onSetPage(currentPage - 2);
				}}
			>
				<Trans>Edellinen</Trans>
			</PaginationLink>
		);
	}

	if (currentPage !== props.totalPagesCount) {
		forwardbutton = (
			<PaginationLink className="pagination-arrow" next onClick={() => props.onSetPage(currentPage)}>
				<Trans>Seuraava</Trans>
			</PaginationLink>
		);
	}

	return (
		<Pagination aria-label="Page navigation">
			<PaginationItem className="pagination-arrow-li">{backbutton}</PaginationItem>
			<ul className="pagination">
				{newRange.map((v, index) => {
					const isCurrent = v === currentPage;
					const onClick = (ev) => {
						ev.preventDefault();
						props.onSetPage(v - 1);
					};
					return (
						<PaginationItem className={isCurrent ? 'active' : ''} key={index}>
							<PaginationLink
								className="pagination-link"
								onClick={onClick}
								disabled={Number.isInteger(v) ? false : true}
							>
								{Number.isInteger(v) ? v : v}
							</PaginationLink>
						</PaginationItem>
					);
				})}
			</ul>
			<PaginationItem>{forwardbutton}</PaginationItem>
		</Pagination>
	);
};

export default ListPagination;
