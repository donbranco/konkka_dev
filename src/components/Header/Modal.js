import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';


const PopUp = (props) => {
  if(props.isSelected) {
    return (
      <>
        <Modal isOpen={props.isSelected} centered={true} toggle={props.toggle} className='modal-popup'>
          <ModalHeader toggle={props.toggle}>{props.title}</ModalHeader>
          <ModalBody>
            {props.content}
          </ModalBody>
        </Modal>
      </>
    )
  } else {
    return <></>
  }
}

export default PopUp;