import React from 'react'
import { inject, observer } from 'mobx-react'

function Hamburger(props) {
  return (
    <button
      class="hamburger"
      type="button"
      onClick={this.props.layoutStore.toggleMenu()}
    >
      <span class="hamburger-box">
        <span class="hamburger-inner" />
      </span>
    </button>
  );
}

export default inject('layoutStore')(observer(Hamburger))
