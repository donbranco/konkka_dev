import React, { Component } from 'react';
import { NavLink as RouterNavLink, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { Container, Row, Navbar, NavbarBrand } from 'reactstrap';

import authStore from '../../stores/authStore';
// import userStore from "../../stores/userStore";

import LoggedInView from './LoggedInView.js';
import LoggedOutView from './LoggedOutView.js';

class Header extends Component {
	state = {
		isOpen: false,
		width: 0,
		height: 0
	};

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	updateWindowDimensions = () => {
		this.setState({ width: window.innerWidth, height: window.innerHeight });
	};

	toggle = (e) => {
		e.preventDefault();
		this.setState({ isOpen: !this.state.isOpen });
	};

	toggleModal = (e) => {
		e.preventDefault();
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	};

	logout = (e) => {
		e.preventDefault();
		authStore.logout();
	};

	render() {
		const { currentUser } = this.props.userStore
		const { isOpen, width } = this.state;

		let isLoggedIn = currentUser.data
		let uid = currentUser ? currentUser : null
		let active = isOpen ? 'is-active' : '';

		const hamburger = (
			<button className={`hamburger hamburger--collapse ${active}`} type="button" onClick={this.toggle}>
				<span className="hamburger-box">
					<span className="hamburger-inner" />
				</span>
			</button>
		);

		return (
			<div className="header konkka-navbar">
				<Container fluid>
					<Row>
						<Navbar expand="md">
							<NavbarBrand exact to="/" tag={RouterNavLink} className="nav-link">
									[Logo -- Konkka_v3]
							</NavbarBrand>
							{/* display burger or not */}
							{width < 768 ? hamburger : ''}

							{/* check if the user is logged in or not and display header based on that */}
							{isLoggedIn ? 
								<LoggedInView
									logout={this.logout}
									user={uid}
									toggleBurger={this.toggle}
									isOpen={isOpen}
								/>
							: <LoggedOutView isOpen={isOpen} toggleBurger={this.toggle} />
							}
						</Navbar>
					</Row>
				</Container>
			</div>
		);
	}
}

export default inject('userStore')(withRouter(observer(Header)));
