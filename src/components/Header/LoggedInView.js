import React from 'react'
import { NavLink as RouterNavLink, withRouter } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Nav, NavItem, NavLink, Label } from 'reactstrap'

import { Trans } from '@lingui/macro'

import logoutLogo from '../../images/logout.svg'
import profileLogo from '../../images/profile.svg'

const LoggedInView = (props) => {
	let active = props.isOpen ? 'active' : ''

	return (
		<>
			<Nav className={`ml-auto navbar__side ${active}`} navbar onClick={props.toggleBurger}>
				{/* <NavItem>
					<NavLink exact to="/profiili/omat-huudot"  tag={RouterNavLink}>
					<Label className="link link__header">
						omat-huudot
					</Label>
				</NavLink>
				</NavItem> */}
				<NavItem>
					<NavLink exact to="/profiili/omat-huudot"  tag={RouterNavLink}>
						<Label
							className="link link__header"
							onClick={() => {
								active = ''
							}}
						>
							<img src={profileLogo} alt='logout logo' />{props.user.name}
						</Label>
					</NavLink>
				</NavItem>
				<NavItem>
					<NavLink>
						<Label
							className="link link__header"
							onClick={() => {
								props.userStore.forgetUser()
								props.authStore.logout()
							}}
						>
							<img src={logoutLogo} alt='logout logo' />
							<Trans>Kirjaudu ulos</Trans>
						</Label>
					</NavLink>
				</NavItem>
			</Nav>
		</>
	)
}

// export default inject('userStore', 'authStore')(observer(LoggedInView))
export default inject('userStore', 'authStore')(withRouter(observer(LoggedInView)))
