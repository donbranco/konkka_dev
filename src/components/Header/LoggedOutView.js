import React, { Component } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';

// language trans
import { Trans } from '@lingui/macro'

import Modal from './Modal';
import LoginForm from '../../pages/Auth/Login/LoginForm';
import Registerform from '../../pages/Auth/Login/RegisterForm';

import loginLogo from '../../images/login.svg';
import registerLogo from '../../images/register.svg';

class LoggedOutView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: false,
      register: false
    };
  }

  toggleLogin = (e) => {
    e.preventDefault();
    this.setState({ login: !this.state.login });
  };

  toggleRegister = (e) => {
    e.preventDefault();
    this.setState({ register: !this.state.register });
  };

  registerModal = () => {
    const register = this.state.register
    if(register) return (
      <Modal
        isSelected={register}
        title="Rekisteröidy"
        toggle={this.toggleRegister}
        content={<Registerform />}
      />
    )
  }

  loginModal = () => {
    const login = this.state.login
    if(login) return (
      <Modal
        isSelected={login}
        title="Kirjaudu sisään"
        toggle={this.toggleLogin}
        content={<LoginForm />}
      />
    )
  }

  render() {
    // const { register, login } = this.state;
    let active = this.props.isOpen ? 'active' : '';

    return (
      <>
        <Nav className={`ml-auto navbar__side ${active}`} navbar onClick={this.props.toggleBurger}>
          <NavItem>
            <NavLink className="link link__header" onClick={this.toggleRegister}>
              <img src={registerLogo} alt='logout logo' />
                <Trans>Rekisteröidy</Trans>
            </NavLink>
              {this.registerModal()}
          </NavItem>
          <NavItem>
            <NavLink className="link link__header" onClick={this.toggleLogin}>
              <img src={loginLogo} alt='logout logo' />
              <Trans>Kirjaudu sisään</Trans>
            </NavLink>
              {this.loginModal()}
          </NavItem>
        </Nav>
      </>
    );
  }
}

export default LoggedOutView;
