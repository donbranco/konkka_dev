import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { SnackbarProvider, withSnackbar } from 'notistack'

// class App extends React.Component {
function MessageHandler(props) {
  const variant = props.variant
  const message = props.message
  const duration = { autoHideDuration: 5000 }
  // const onExited = { onExited: props.clear }

  switch (variant) {
    case 'success':
      props.enqueueSnackbar( message, { variant });
      return null;
    case 'warning':
      props.enqueueSnackbar( message, { variant });
      return null;
    case 'error':
      props.enqueueSnackbar( message, { variant });
      return null;
    default:
      props.enqueueSnackbar(message, { variant });
      return null;
  }
}

const Message = withSnackbar(MessageHandler);



// make class thingy
class Notification extends Component {
  render() {
    const { alert, type, message, clear } = this.props
    const width = this.props.layoutStore.screenWidth
    const density = width < 992 &&  width !== null ? true : false
  
    if(alert) {
      return (
        <div className={`snack__notification`}>
          <SnackbarProvider
              maxSnack={3} 
              dense={density}
              preventDuplicate={true} 
              // onExit={clear}
              autoHideDuration={5000}
              >
            <Message variant={type} message={message} />
          </SnackbarProvider>
        </div>
      )
    } return <></>
  }
}

// export default Notification

export default inject('layoutStore')(observer(Notification))