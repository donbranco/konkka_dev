import React from 'react'
import { inject, observer } from 'mobx-react'
import { NavLink as RouterNavLink, withRouter } from 'react-router-dom'
import { Container, Row, Navbar, NavItem, NavLink, Label } from 'reactstrap'

// TODO: create a simple page this inherites the body content and display that

function StaticHeader({ uid, pages, history }) {
  const { setLanguageSetting } = this.props.commonStore
  const languageStyle = {
    fontSize: 16,
    width: 50,
    color: 'white',
  }

  const alignCenter = {
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center'
  }

  if(typeof pages !== 'undefined' && pages.length > 0 ) {
    const staticHeaders = pages.map((page, i) => {
      return (
        <NavItem key={i}>
          <NavLink exact to={'/page/' + page.nid }  tag={RouterNavLink}>
            <Label className="link link__header"
              onClick={() => history.push({ pathname: '/page/' + page.nid })}
            >
                { page.title }
            </Label>
          </NavLink>
        </NavItem>
      )
    })

    return (
      <div className="static-header header ">
        <Container fluid>
          <Row>
            <div>
              <Navbar className="static-navbar">
                { staticHeaders }
              </Navbar>
            </div>
            <div style={alignCenter}>
              <div style={languageStyle} onClick={() => setLanguageSetting('fi')}>
                <Label>fi</Label>
              </div>
              <div style={languageStyle} onClick={() => setLanguageSetting('nl')}>
                <Label>nl</Label>
              </div>
            </div>
          </Row>
        </Container>
      </div>         
    )
  }
  return <></>
}
// export default withRouter(StaticHeader)
export default inject('commonStore')(withRouter(observer(StaticHeader)))