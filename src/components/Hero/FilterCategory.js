import React from 'react';
import { observer, inject } from 'mobx-react';
import { Col } from 'reactstrap';

function FilterCategory({ selectedCategory, currentTag }) {
	const categories = this.props.layoutStore.categories

	if(categories) {
		return categories.map((category, index) => {
			if(category.parent_id === '') {
				const active = category.id === currentTag ? 'active' : null;
				return (
					<Col
						key={index}
						id={category.id}
						className={`hero-filter ${active}`}
						onClick={selectedCategory}
					>
						{category.name}
					</Col>
				);
			} return <></>
		});
	}
	return <></>
}

export default inject('layoutStore')(observer(FilterCategory))