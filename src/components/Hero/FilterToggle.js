 import React from 'react'

	// import filterImageOrange from '../../images/filter_icons/filterorange.svg'
	import filterImageBlue0 from '../../images/filter_icons/filter.svg'
	import filterImageBlue1 from '../../images/filter_icons/001.svg'
	import filterImageBlue2 from '../../images/filter_icons/002.svg'
	import filterImageBlue3 from '../../images/filter_icons/003.svg'
	import filterImageBlue4 from '../../images/filter_icons/004.svg'
	import filterImageBlue5 from '../../images/filter_icons/005.svg'
	import filterImageBlue6 from '../../images/filter_icons/006.svg'
	import filterImageBlue7 from '../../images/filter_icons/007.svg'
	import filterImageBlue8 from '../../images/filter_icons/008.svg'
	import filterImageBlue9 from '../../images/filter_icons/009.svg'
	// import filterImageBlue10 from '../../images/filter_icons/010.svg'

export default function FilterToggle({ toggle, collapse, toggleText, width, totalSelectedFilters }) {
	const displayText = width > 992 ? toggleText : ''

	let filter_image = filterImageBlue0
	if(totalSelectedFilters === 0) filter_image = filterImageBlue0
	if(totalSelectedFilters === 1) filter_image = filterImageBlue1
	if(totalSelectedFilters === 2) filter_image = filterImageBlue2
	if(totalSelectedFilters === 3) filter_image = filterImageBlue3
	if(totalSelectedFilters === 4) filter_image = filterImageBlue4
	if(totalSelectedFilters === 5) filter_image = filterImageBlue5
	if(totalSelectedFilters === 6) filter_image = filterImageBlue6
	if(totalSelectedFilters === 7) filter_image = filterImageBlue7
	if(totalSelectedFilters === 8) filter_image = filterImageBlue8
	if(totalSelectedFilters === 9) filter_image = filterImageBlue9



	if(!collapse) return <span className={`search-form__button`} onClick={toggle}><img src={filter_image} alt="filter logo"></img>{displayText}</span>
	if(collapse) return  <span className={`search-form__button`} onClick={toggle}><img src={filter_image} alt="filter logo orange"></img>{displayText}</span>

}