import React from 'react'
import { Input } from 'reactstrap'
import { Trans } from '@lingui/macro'


export default function AuctionTypeFilter({ filterSubmission, auctionStatus }) {
  const _checkbox_list = [ {option: 'Online auction', type: 1 }, { option: 'Online Product', type: 2 }, {option: 'Onsite auction', type: 3} ]
 
  const auction_types = _checkbox_list.map((checkbox, i) => {
    const box = checkbox.option;
    const type = checkbox.type
    const checkbox_option = box.charAt(0).toUpperCase() + box.slice(1)    
    return <option key={i} value={type} selected={auctionStatus === type}>{checkbox_option}</option>
  })

  const all_types = <Trans><option key={new Date()} value={0} selected={auctionStatus === 0 || auctionStatus === '0'}>Show all</option></Trans>

  return (
    <Input
    type="select"
    name="auction-types"
    id="auction-types"
    onChange={filterSubmission}
    >
      {all_types}
      {auction_types} 
    </Input>
  )
}