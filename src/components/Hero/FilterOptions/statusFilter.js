import React from 'react'
import { Input } from 'reactstrap'
import { Trans } from '@lingui/macro'


export default function StatusFilter({ filterSubmission , filterStatus }) {
  const options = [ 
    { type: 'Active' }, 
    { type: 'Ended' }
  ]

  return (
    <Input
      type="select"
      name="auction-status"
      id="auction-status"
      onChange={filterSubmission}
    >
      {
        options.map((option, i) => {
          const status = filterStatus.charAt(0).toUpperCase() + filterStatus.slice(1);
          const { type } = option
      
          if(type === 'Active') return <Trans><option key={i} value={type} selected={status === type}>Active</option></Trans>
          if(type === 'Ended') return <Trans><option key={i} value={type} selected={status === type}>Ended</option></Trans>
        })
      } 
    </Input>
  )
}