import React, { Component } from 'react'
import { Input } from 'reactstrap'
import { Trans } from '@lingui/macro'

export default class pricingFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      priceRange: [0, 10, 20, 50, 100, 150, 200, 500, 1000, 2000, 5000, 10000, 'No limit']
    }
  }

  generateOptions = (group, current_value) => {
    const options = this.state.priceRange.map((amount, i) => {
      // removes no limit from min price
      if(group === 'min-price' && amount === 'No limit') {
        return null
      } else if(group === 'min-price' && amount === 0) {
        return <Trans><option key={i} name={group} value={amount} index={i} className={`${group}-selected`} >Min price</option></Trans>
        // removes 0 from max price
      } else if(group === 'min-price' && amount) {
        return <option key={i} name={group} value={amount} index={i} className={`${group}-selected`} >{amount}</option>
        // removes 0 from max price
      } else if(group === 'max-price' && amount === 0) {
        return null
      } else if(group === 'max-price' && amount === 'No limit') {
        return <Trans><option key={i} name={group} value={amount} index={i} className={`${group}-selected`}>Max price</option></Trans>
      } 
      else {
        // somehow sets 0 for min price
        return <option key={i} name={group} value={amount} index={i}>{amount}</option>
      }
    });
    return options;
  }


  render() {
    const { filterSubmission, startingPrice, endingPrice } = this.props;
    const group_starting = 'min-price';
    const group_ending = 'max-price';

    return (
      <div className="filter__flex no-padding col-sm-12 price col-md-12">
        <div className="filter__input-rounded price small">
          <Input
            value={startingPrice}
            className=""
            type="select"
            name="min-price"
            id="startingPrice"
            placeholder="min"
            onChange={filterSubmission}
          >
          {this.generateOptions(group_starting, startingPrice)}
          </Input>
        </div>

        <div className="filter__input-rounded  price small">
          <Input
            value={endingPrice}
            className=""
            type="select"
            name="max-price"
            id="endingPrice"
            placeholder="max"
            onChange={filterSubmission}
          >
          {this.generateOptions(group_ending, endingPrice)}
          </Input>
        </div>
      </div>
    )
  }
}
