import React from 'react'
import { Input, Label, FormGroup } from 'reactstrap'

export default function categoryFilter({ filterSubmission, categories, parentID, show, disableFilters }) {

  let match_exists = false;

  if(categories && parentID) {
    let _first_option = true;
    const _subCategories = categories.map((category, i) => {
      const match = parentID.toString() === category.parent_id ? true : false

      if(match) {
        match_exists = true;

        if(_first_option) {
          _first_option = false;
          const _subCategory = category.parent_id === parentID ? <option key={i} value={category.id}>{category.name}</option> : null;
          return (
            <>
              <option value={false}>All</option> 
              {_subCategory}
            </>
          );
        }
        if(category.parent_id === parentID) return <option key={i} value={category.id}>{category.name}</option>
      }
      return null
    }); // end map


    const input = ( 
      <fieldset disabled={disableFilters}>
        <div className="filter__input-rounded">
          <Input
            type="select"
            name="subcategory"
            id="subcategory"
            onChange={filterSubmission}
            >
            {_subCategories} 
          </Input>
        </div>
      </fieldset>
    )

    const input_desk = ( 
      <FormGroup id='subcategory' className="filter__group sub col-sm-12 col-md-12 col-lg-12">
        {input}
      </FormGroup>
    )

    const input_mob = ( 
      <FormGroup id='subcategory' className="filter__group sub col-sm-12 col-md-12 col-lg-12">
        <Label className="filter__search-label" for="sub-category">Sub-categories</Label>
        {input}
      </FormGroup>
    )

    if(match_exists) {
      if(show) return input_desk
      if(!show) return input_mob
	    return <></>      
    }
  } 
  return  <></>      
}
