import React from 'react'
import { Input } from 'reactstrap'
import { Trans } from '@lingui/macro'

export default function CityFilter({ filterSubmission, cities, currentCity }) {
  if(cities) {
    // set all city options
    const _cities = cities.map((city, i) => {
      const _city = city.city
      return <option key={i} id={_city} value={_city} selected={_city === currentCity}>{`${_city}`}</option>
    })

    // set All city option
    const all_cities = <Trans><option value={''} selected={currentCity === null || currentCity === ''}>Show all</option></Trans>

    return (
      <>
        <Input
        type="select"
        name="location"
        id="location-filter"
        onChange={filterSubmission}
        >
          {all_cities}
          {_cities}
        </Input>
      </>
    )  
  }
	return <></>
}
