import React from 'react'
import { Input, Label } from 'reactstrap'
import { Trans } from '@lingui/macro'

export default function anyBidAccepted({ filterSubmission, filterAnyPrice, flexEnd, disableFilters }) {
  const active = filterAnyPrice === '1' ? true : false
  return (
    <fieldset disabled={disableFilters}>
      <div className={`filter__any-bid-wrapper ${flexEnd}`}>
        <div className='filter__any-bid-input'>
          <Input
            type="checkbox"
            name="any_price"
            id="any_price"
            checked={active}
            onChange={filterSubmission}
          />
          <Label name="any_price" onClick={(e) => filterSubmission({currentTarget:{"name": "any_price", "value": active}})}><Trans>No minimum price</Trans></Label>
        </div>
      </div>
    </fieldset>
  )
}