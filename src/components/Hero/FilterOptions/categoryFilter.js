import React from 'react'
import { Input } from 'reactstrap'
import { Trans } from '@lingui/macro'

export default function categoryFilter({ categories, filterSubmission, parentID }) {
  if(categories || parentID) {
    const _categories = categories.map((category,i) => {
      const { id, name, parent_id } = category;
      if(parent_id === '') {
        return <option key={i} value={id} selected={parentID === parent_id}>{name}</option>
      }
      return null
    })

    const all_categories = <Trans><option key={new Date()} value={null} selected={parentID === null || parentID === "Show all" }>Show all</option></Trans>

    return (
      <Input type="select" name="category" id="category" onChange={filterSubmission}>
        {all_categories}
        {_categories}
      </Input>
    )
  }
  return null
}
