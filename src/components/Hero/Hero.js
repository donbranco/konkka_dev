import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import { Container, Col, Row, Form, FormGroup, Input, Collapse } from 'reactstrap'

// language trans
import { Trans } from '@lingui/macro'

import FilterToggle from './FilterToggle'
import AdvancedFilters from './AdvancedFilters'
import Categories from './FilterCategory'

import SearchIcon from '../../images/search.svg'

import './Hero.css'

class Hero extends Component {
	constructor(props) {
		super(props)
		this.state = {
			collapse: null,
			width: null,
			height: null,
		}

		this.searchInputHandler = this.searchInputHandler.bind(this)
		this.submitSearch = this.submitSearch.bind(this)
	}

	componentWillMount = async () => {
		this.setInitialWidth()
		await this.props.layoutStore.getTags()
		this.props.layoutStore.getLocations()
		this.setState({ collapse: false })
	}

	componentDidMount = () => window.addEventListener('resize', this.updateWindowDimensions)

	setInitialWidth = () => this.setState({ width: window.innerWidth, height: window.innerHeight })

	updateWindowDimensions = () => this.setState({ width: window.innerWidth, height: window.innerHeight })

	searchInputHandler = (e) => this.props.productStore.setFilterData('search-text', e.target.value)

	submitSearch = (e) => {
		e.preventDefault()
		this.props.productStore.clearPagination()
		this.props.productStore.getProductDetailList('advancedFilter')
	}

	selectedCategory = (e) => {
		// setting filter category selection
		const categoryID = e.currentTarget.id
		this.props.productStore.clearPagination()
		this.props.productStore.setFilterData('category', categoryID)
		this.props.productStore.getProductDetailList('advancedFilter')

		// counting filter selectino
		this.props.layoutStore.setFilterCounter()
		// display advanced filters
		if(!this.state.collapse) this.showAdvancedSearch()
	}
	
	showAdvancedSearch = () => this.setState({ collapse: !this.state.collapse })

	render() {
		// TODO: error handling with Mobx if needed and not internal state
		const { collapse, width } = this.state
		const title = this.props.title
		const textFilter = this.props.productStore.filterText
		const { categories, totalActiveFilters } = this.props.layoutStore
		const { filterTag } = this.props.productStore

		const ToggleFilterText = collapse ? <Trans>Show filters</Trans> : <Trans>Filters</Trans>
		// const search_placeholder = <Trans>Mitä etsit?</Trans>
		
		return (
			<div className="hero">
				<Container fluid className="hero-container">
					<h1 className="page-title"><Trans>{title}</Trans></h1>
					<Row>
						<div className="hero-filters">
							<Collapse className="hero-filters-category" isOpen={!collapse}>
								<div className="items">
									<Categories
										currentTag={filterTag}
										categories={categories}
										selectedCategory={this.selectedCategory}
									/>
								</div>
							</Collapse>
						</div>
					</Row>
					<Row>
						<Col md={12} sm={12}>
							<Form className="search-form" onSubmit={this.submitSearch}>
								<FormGroup className="search-form__flex">
									<span className="search-icon"><img src={SearchIcon} alt="search icon" /></span>
									
										<Input
											type="search"
											value={textFilter}
											onChange={this.searchInputHandler}
											name="search"
											id="heroSearch"
											placeholder='Mitä etsit?'
										/>
									
									<FilterToggle collapse={collapse} toggle={this.showAdvancedSearch} toggleText={ToggleFilterText} width={width} totalSelectedFilters={totalActiveFilters} />
								</FormGroup>
							</Form>
						</Col>
					</Row>
					<Row>
						<Col md={12} sm={12}>
							<AdvancedFilters collapse={collapse} />
						</Col>
					</Row>
				</Container>
			</div>
		)
	}
}

export default inject('productStore', 'layoutStore')(withRouter(observer(Hero)))
