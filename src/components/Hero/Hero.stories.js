import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import Hero from './Hero';

const stories = storiesOf('Hero', module);

stories.addDecorator(withKnobs);

export const hero = {
title: 'Konkurssihuutokauppa',
paragraph: 'Konkan upouudet sivut',
};

stories
  .add('default', () => <Hero title={text('Title', hero.title)} paragraph={text('Paragraph', hero.paragraph)} />)