import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Jumbotron, Container, Form, FormGroup, Label, Input } from 'reactstrap';

import './Hero.css';

class Hero extends Component {

  constructor(props) {
    super(props);
    this.state = {
      search_text: ''
    }

    this.searchInputHandler = this.searchInputHandler.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
  }

  searchInputHandler (e) {
    let searchText = e.target.value;
    this.setState({
      search_text: searchText
    })
  }

  submitSearch(event) {
    event.preventDefault();
    console.log('Submitting form data..');
    this.props.productStore.loadProducts('search_text', null, this.state.search_text);
  }

  render () {
    return (
      <Jumbotron fluid className="Hero">
        <Container fluid>
          <h1 className="display-3">{this.props.title}</h1>
          <p className="lead">{this.props.paragraph}</p>

          <Form onSubmit={this.submitSearch}>
            <FormGroup>
              <Label for="exampleSearch">Hae tuotteita</Label>
              <Input type="search" value={this.state.search_text} onChange={this.searchInputHandler} name="search" id="exampleSearch" placeholder="Hae tuotteen nimellä" />
            </FormGroup>
          </Form>
          
        </Container>
      </Jumbotron>
    );
  }
}

export default inject('productStore')(observer(Hero));