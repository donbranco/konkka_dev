import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Collapse, Label, Form, FormGroup, Button} from 'reactstrap'

import { Trans } from '@lingui/macro'

// filters
import CitiesFilter from './FilterOptions/cityFilter'
import CategoryFilter from './FilterOptions/categoryFilter'
import SubCategoryFilter from './FilterOptions/subCategoryFilter'
import PricingFilter from './FilterOptions/pricingFilter'
import AuctionTypeFilter from './FilterOptions/auctionTypeFilter'
import StatusFilter from './FilterOptions/statusFilter'
import AnyBidAcceptedCheckBox from './FilterOptions/anyBidAccepted.js'


import ResetButton from '../../images/reset.svg'

class FilterDropDown extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dropdownOpen: false,
      splitButtonOpen: false,
      category: 'Category',
      width: 0, height: 0,
    }
  }

  componentDidMount = () => {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount = () => window.removeEventListener('resize', this.updateWindowDimensions)

  updateWindowDimensions = () => this.setState({ width: window.innerWidth, height: window.innerHeight })

  toggleDropDown = () => this.setState({ dropdownOpen: !this.state.dropdownOpen })

  toggleSplit = () => this.setState({ splitButtonOpen: !this.state.splitButtonOpen })

  filterSubmission = e => {
    const { 
      clearTotalProducts,
      clearPagination,
      setFilterData,
      getProductDetailList
    } = this.props.productStore
    
    clearTotalProducts()

    const filter = e.currentTarget
    const filter_name = filter.name
    const input = filter.value


    if(filter_name.toString() === 'reset-filters') {
      setFilterData(filter_name, input)
    } else {
      clearPagination()
      setFilterData(filter_name, input)
      getProductDetailList('advancedFilter')
    }
    // set filter counter
    this.props.layoutStore.setFilterCounter()
  }

  render() {
    const { categories, cities } = this.props.layoutStore
    const { collapse } = this.props
    const { 
			filterStartingPrice,
			filterEndingPrice,
			filterLocation,
			filterAuctionType,
			filterStatus,
			filterAnyPrice,
			disableFilters
		} = this.props.productStore

    const filterID = this.props.productStore.filterTag
    const { width } = this.state

    let position = document.getElementById('subcategory') === null ? '' : 'end'

    if(cities !== false && categories) {

      const _subCategoriesComponent = <SubCategoryFilter filterSubmission={this.filterSubmission} categories={categories} parentID={filterID} show={true} disableFilters={disableFilters}/>
      const _subCategoriesComponentMob = <SubCategoryFilter filterSubmission={this.filterSubmission} categories={categories} parentID={filterID} show={false} disableFilters={disableFilters}/>


      // let spaceBetweenResetAndSub = document.getElementById('subcategory') && width >= 992 ? 'space-between' : 'flex-end'

      return (
        <>
          <Collapse className="" isOpen={collapse}>
            <Form className="filter filter__flex">
              {/* categories */}
              <FormGroup className="filter__group col-sm-12 col-md-12 col-lg-3">
                <Label className="filter__search-label" for="Category">
                  <Trans>Category</Trans>
                </Label>
                <fieldset disabled={disableFilters}>
                  <div className="filter__input-rounded">
                    <CategoryFilter categories={categories} filterSubmission={this.filterSubmission} parentID={filterID}/>
                  </div>
                </fieldset>
                { width < 992 ?_subCategoriesComponentMob : _subCategoriesComponent}
              </FormGroup>
              {/* price range  */}
              <FormGroup className="filter__group col-sm-12 col-md-6 col-lg-3">
                <Label className="filter__search-label" for="price-range">
                <Trans>Price range</Trans>
                </Label>
                <fieldset disabled={disableFilters}>
                  <PricingFilter filterSubmission={this.filterSubmission} startingPrice={filterStartingPrice} endingPrice={filterEndingPrice}/>
                </fieldset>
                  <AnyBidAcceptedCheckBox 
                    filterSubmission={this.filterSubmission} 
                    filterAnyPrice={filterAnyPrice} 
                    flexEnd={position}
                    disableFilters={disableFilters} 
                  />
              </FormGroup>
              {/* input type */}
              <FormGroup className="filter__group col-md-6 col-lg-2 col-sm-6" check>
                <Label className="filter__search-label" for="price-range">
                    <Trans>Auction type</Trans>
                </Label>
                <fieldset disabled={disableFilters}>
                  <div className="filter__input-rounded">
                    <AuctionTypeFilter filterSubmission={this.filterSubmission} auctionStatus={filterAuctionType}/>
                  </div>
                </fieldset>
              </FormGroup>
              {/* <FormGroup className="filter__group col-md-4 col-lg-2 col-sm-6" check> */}
              <FormGroup className="filter__group col-md-6 col-lg-2 col-sm-6" check>
                <Label className="filter__search-label" for="price-range">
                    <Trans>Status</Trans>
                </Label>
                <fieldset disabled={disableFilters}>
                  <div className="filter__input-rounded">
                    <StatusFilter filterSubmission={this.filterSubmission} filterStatus={filterStatus} />
                  </div>
                </fieldset>
              </FormGroup>
              {/* location / cities */}
              <FormGroup className="filter__group col-sm-12 col-md-6 col-lg-2">
                <Label className="filter__search-label" for="Location">
                  <Trans>Location</Trans>
                </Label>
                <fieldset disabled={disableFilters}>
                  <div className="filter__input-rounded">
                    <CitiesFilter cities={cities} filterSubmission={this.filterSubmission} currentCity={filterLocation}/>
                  </div>
                </fieldset>
                <div className="reset-filters">
                  <Button className='col-xs-12' name='reset-filters' onClick={this.filterSubmission}>
                    <img src={ResetButton} alt="reset button" />
                    <Trans>Reset all filters</Trans>
                  </Button>
                </div>
              </FormGroup>
            </Form>
          </Collapse>
        </>
      )
    } else {
      return null
    }
  }
}

export default inject('productStore', 'layoutStore')(observer(FilterDropDown))
