import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, radios } from '@storybook/addon-knobs';

import Button from './Button';

const stories = storiesOf('Button', module);

stories.addDecorator(withKnobs);

export const button = {
  title: 'Click me',
  color: 'primary',
  state: 'BUTTON_STATE'
};

export const actions = {
  onButtonClick: action('onButtonClick')
};

stories
  .add('default', () => <Button color={button.color} {...actions} title={text('Title', button.title)} />)
  .add('danger', () => <Button color="danger" {...actions} title="Im a custom title" />)