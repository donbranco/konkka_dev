import React from 'react';
import { Button } from 'reactstrap';

import styles from './Button.module.css';

const button = (props) => {
  return (
    <div>
      <Button
        className={styles.Button}
        onButtonClick={props.onButtonClick} 
        color={props.color}>{props.title}</Button>
    </div>
  );
};

export default button;