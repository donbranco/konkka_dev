import React from "react"
import { Row, Container } from 'reactstrap'
import { Trans } from '@lingui/macro'

const footer = () => {
  return (
    <Container fluid className="footer-wrapper">
      <Row>
        <footer className="col-12 footer">
          <div className="footer-top">
            <span className="footer-logo">LOGO</span>
            <span className="footer-some">
              {/* <button href="#">SOME</button> */}
             <Trans><a href="#">SOME</a></Trans>
            </span>
          </div>
          <div className="footer-bottom">
            <span className="footer-newsletter">
              {/* <button href="#">Tilaa uutiskirje</button> */}
              <Trans><a href="#">Tilaa uutiskirje</a></Trans>
            </span>
            <span className="footer-menu">
              <nav>
                <ul>
                  <li>
                    <Trans><a href="#">Etusivu</a></Trans>
                    {/* <button>Etusivu</button> */}
                  </li>
                  <li>
                    <Trans><a href="#">Käyttöehdot</a></Trans>
                    {/* <button>">Käyttöehdot</button> */}
                  </li>
                  <li>
                    <Trans><a href="#">Palaute ja kysymykset</a></Trans>
                    {/* <button>Palaute ja kysymykset</button> */}
                  </li>
                  <li>
                    {/* <button>Rekisteriseloste</button> */}
                    <Trans><a href="#">Rekisteriseloste</a></Trans>
                  </li>
                  <li>
                    {/* <button>Tietoa palvelusta</button> */}
                    <Trans><a href="#">Tietoa palvelusta</a></Trans>
                  </li>
                  <li>
                    {/* <button>Yritys</button> */}
                    <Trans><a href="#">Yritys</a></Trans>
                  </li>
                </ul>
              </nav>
            </span>
          </div>
        </footer>
      </Row>
    </Container>
  );
};

export default footer;
