import React, { Component } from "react";
import { Container, Row } from "reactstrap";

export default class Layout extends Component {
  state = {
    containerFluid: false
  };

  render() {
    return (
      // <Container fluid className={this.props.containerClass} fluid={this.props.fluid}>
      <Container fluid>
        <Row>{this.props.children}</Row>
      </Container>
    );
  }
}
