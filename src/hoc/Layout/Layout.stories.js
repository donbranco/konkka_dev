import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Col } from 'reactstrap';

import Layout from './Layout';
import Card from '../../components/Card/Card';

const stories = storiesOf('Layout', module);

stories.addDecorator(withKnobs);

stories
  .add('default', () => (
  <Layout>
    <Card title="Card #1" subtitle="Subtitle" price="100" timeLeft="30 sec" />
  </Layout>))
    .add('withThreeCards', () => (
      <Layout>
        <Col md="4">
          <Card title="Card #1" subtitle="Subtitle" price="100,00 €" timeLeft="30 sec" />
          <Card title="Card #2" subtitle="Subtitle" price="50,00 €" timeLeft="15 min 30 sec" />
          <Card title="Card #3" subtitle="Subtitle" price="25,00 €" timeLeft="1 hour 30 min" />
        </Col>
      </Layout>))