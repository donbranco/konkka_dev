import { observable, action, decorate, configure, runInAction, autorun } from 'mobx'

import userStore from './userStore'
import productStore from './productStore'
import api from '../api';

configure({ enforceActions: 'observed' })

class automatedBidProcess {
  // init val of observables
  automatedBiddingProcess = false
  maxBid = null
  placedBid = null
  increment = null
  pidd = null

  // inint automated bidding process
  setAutomatedBid = async ({ max, pid, increment }) => {
    this.pid = pid
    this.increment = increment
    // parseint may not be needed
    this.maxBid = parseInt(max)
    this.automatedBiddingProcess = true
  }

  // stop automated bidding process
  clearAutomatedBid = () => {
    this.automatedBiddingProcess = false
    this.maxBid = null
  }

  // stop automated bid process
  stopAutomatedBid = () => {
    this.automatedBid()
    this.clearAutomatedBid()
  }


  // start automated bid process
  startAutomatedBid = () => {
    const { pid, increment, maxBid } = this
    // continuously checks for new bids and places new bids
    this.automatedBid = autorun( async () => {

      console.log('check continuity of the autorun')

      if(this.automatedBiddingProcess) {

        console.log('automated bid is active....')

        // TODO: check whether or not uid is included
        const uid = userStore.currentUser.uid
        const currentHighestBid = await api.Bids.listBidsByHighest(pid)
        const newBidValue = currentHighestBid.bid_amount + increment
        // if the current highest bid is not placed by user and is lower than maximum bid, place a new bid.
        if(currentHighestBid.uid !== uid && newBidValue < maxBid) {
          
          // ? does react state need to be updated or not??
          // format new bid data for node

          const data = {
            pid: pid,
            uid: uid,
            bid_amount: newBidValue
          }

          console.log('send new bid: ', data)

          this.sendNewBid(data)
        } else {
          this.stopAutomatedBid()
        }
      }  
    }, {
        onError(e) {
            window.alert("Please enter a valid bid", e)
        }
    })
  }

  sendNewBid = (data) => {
    productStore.sendBid(data)
  }
}

decorate(automatedBidProcess, {
  automatedBiddingProcess: observable,
  pid: observable,
  maxBid: observable,
  placedBid: observable,
  increment: observable,

  setAutomatedBid: action,
  startAutomatedBid: action,
  stopAutomatedBid: action,
})

const automatedBid = new automatedBidProcess()
window.bid = automatedBid
export default automatedBid
