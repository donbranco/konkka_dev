import authStore from './authStore';
import userStore from './userStore';
import commonStore from './commonStore';
import productStore from './productStore';
import profileStore from './profileStore';
import layoutStore from './layoutStore';

const stores = {
	authStore,
	userStore,
	commonStore,
	productStore,
	profileStore,
	layoutStore
};

export default stores;
