import { observable, action, decorate, configure, runInAction } from 'mobx';

import userStore from './userStore';
import api from '../api';

configure({ enforceActions: 'observed' });

class ProfileStore {
  // TODO:
  // create an Array or Map contain all auctions
  // here a bid is placed. Use that info to display table
  placedBids;
  product_data = false;

  // all the active auctions on Node
  total_auctions = 0;
  active_auctions = false;
  all_active_auctions = false;

  // favourites -- tracking auctions
  favourites_auctions = [];

  // * ----- Placed bids on auctions ------

  getAllProducts = async () => {
    return await api.Profile
      .getAllProducts()
      .then((res) => {
        this.all_active_auctions = res.data;
        return res.data;
      })
      .catch((e) => {
        console.error(e);
      });
  };

  // find all the bids where the user has placed a bid
  setActiveAuctionResults = async (auctions) => {
    const _uid = userStore.currentUser.uid;
    let _bids = [], _pids = [], _active_auctions = [], _found = false;

    auctions.map(auction => {
      // check from the highest bid where users bid is location
      auction.bids.slice(0).reverse().map(bid => {
        if (bid.uid === _uid && _found !== true) {
          _found = true;
          _bids.push(bid);
          console.log('push pids: ', parseInt(auction.pid));
          _pids.push(auction.pid);
          _active_auctions.push(auction);
          console.log('bid that is placed: ', bid)
          return bid
        }
      });
      _found = false;
      return auction
    });
    console.log('Setting pids: ', _pids);
    this.setActiveResult(_active_auctions, _bids, _pids);
  };

  setActiveResult = async (active_auctions, bids_placed, pids) => {
    api.Products.productDetails({ product_ids: pids })
    .then((res) => {
      if (res) {
        runInAction(() => {
          console.log(res);
          this.product_data = res;
          this.active_auctions = active_auctions;
          this.placed_bids = bids_placed;
        });
      }
    });
  };

  // * ------------------------------------

  // ! ------------- separation block ------------

  // * ----- get the data for favourites ------
  getFavourites = () => {
    const _favourites = userStore.favourite_ids.map(pid => {
      const bid = this.getHighestPlacedBid(pid);
      // check if there has been a bid placed and tracked by Node
      if (typeof bid !== 'undefined') return bid
      // get bid data from Drupal --> no bids placed yet
      return console.log('Big data is coming from Drupal');
    });
    this.favourites = _favourites;
  };

  getHighestPlacedBid = async pid => {
    // get active bids from Node API call, where user placed a bid.
    return await api.Bids.listBidsByHighest(pid);
  };
}

decorate(ProfileStore, {
  active_auctions: observable,
  all_active_auctions: observable,
  favourites_auctions: observable,
  getPlacedBids: action,
  getAllProducts: action,
  placedBids: observable,
  product_data: observable,
  setActiveAuctionResults: action,
  setAuctionsWithPlacedBids: action
});

const profileStore = new ProfileStore();
window.profileStore = profileStore;
export default profileStore;
