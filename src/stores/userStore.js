import { observable, action, decorate, runInAction } from 'mobx'

// import productStore from './productStore'
import authStore from './authStore'

import api from '../api'

class UserStore {
  currentUser = false
  loadingUser = false

  pullUser = () => {
    const token_validation_cookie = authStore.user_auth_cookie.value

    if(typeof token_validation_cookie !== 'undefined' && this.currentUser === false ) {
      this.loadingUser = true
      console.log('pulling user...... ')

      // user login when cookie is set on load
      api.Auth.current()
      .then(user => {
          console.log('pulled currentUser data: ')
          runInAction(() => {
            this.currentUser = user.data
            this.loadingUser = false
          })
        })
        .catch(error => {
          // if(window.localStorage.user_auth_token) window.localStorage.removeItem('user_auth_token')
          this.loadingUser = false
          authStore.unsetUserCookie()
          // productStore.setNotification('error', 'not able to loggin user from current session')

          console.log('pull failed. Token removed: ')
          console.log(error)
        });
    } else {
      // if(window.localStorage.user_auth_token) window.localStorage.removeItem('user_auth_token')
      console.log('Automated user login did not work -- tokens are not set')
    }
  } 

  forgetUser() {
    this.currentUser = false
  }
}

decorate(UserStore, {
  currentUser: observable,
  loadingUser: observable,
  uid: observable,
  pullUser: action,
  forgetUser: action
})

const userStore = new UserStore()
window.user = userStore

export default userStore
