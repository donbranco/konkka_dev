import { observable, action, decorate, computed, runInAction } from 'mobx'
import Cookie from 'mobx-cookie'
import userStore from './userStore'
import commonStore from './commonStore'

import api from '../api'
import productStore from './productStore'

class AuthStore {
  user_auth_cookie = new Cookie('user_authentication_token')

  // might not be needed any more -- long form of testing and checking the flow
  user_auth_token = ''

  values = {
    username: '',
    password: ''
  }

  setUsername = (username) => this.values.username = username

  setPassword = (password) => this.values.password = password

  setCookie = (token) => { return this.user_auth_token = token }

  unsetCookie = () => { return this.cookie.remove() }

  setUserCookie = res => {
    const { uid, mail } = res.data.user
    const { sessid, session_name, token } = res.data

    let user_data = {
      uid: uid,
      email: mail,
      sessionid: sessid,
      session_name: session_name,
      token: token
    };

    this.user_auth_cookie.set(user_data)
  };

  unsetUserCookie = () => this.user_auth_cookie.remove()

  fetchUserAuthToken = () => this.user_auth_token = this.user_auth_cookie.value

  get getUserCookie() { return this.user_auth_token}

  register = () => {
    return api.Auth
      .register(this.values)
      .then(res => {
        const { uid } = res.data.user
        
        this.setUserCookie(res)
        console.log('setting user token..')
        commonStore.setToken(uid)


        // set obersables --> updates the UI
        runInAction(() => {
          const type = 'success'
          const message = 'You are succesfully logged in!'

          userStore.currentUser = res.data.user
          userStore.loadingUser = false
          productStore.setNotification(type , message)
        })
      })
      .catch(e => {
        runInAction(() => {
          userStore.loadingUser = false

          const type = 'error'
          const message = 'Login fail. Either password or usernam is incorrect!'  
          productStore.setNotification(type , message)
        })
        console.error(e)
      })
  }
  login = () => {
    return api.Auth
      .login(this.values)
      .then(res => {
        const { uid } = res.data.user
        console.log('login resp: ', res)

        this.setUserCookie(res)
        console.log('setting user token..')
        commonStore.setToken(uid)


        // set obersables --> updates the UI
        runInAction(() => {
          const type = 'success'
          const message = 'You are succesfully logged in!'

          userStore.currentUser = res.data.user
          userStore.loadingUser = false
          productStore.setNotification(type , message)
        })
      })
      .catch(e => {
        runInAction(() => {
          userStore.loadingUser = false

          const type = 'error'
          const message = 'Login fail. Either password or usernam is incorrect!'  
          productStore.setNotification(type , message)
        })
        console.error(e)
      })
  }

  logout = async () => {
     api.Auth.logout()
      .then(() => {
      console.log('successfully logged out user')
      console.log('removing cookies and tokens')
      
      // remove all cookies, tokens and user data
      this.unsetUserCookie()
      commonStore.setToken(false)
      window.localStorage.removeItem('user_auth_token')

      // run notifaction pop up snack
      runInAction(() => {
        const type = 'success'
        const message = 'You are successfully logged out!'

        userStore.currentUser = false
        productStore.setNotification(type , message)
      })
    })
    .catch(e => {
      console.error(e)
      runInAction(() => {
        const type = 'error'
        const message = 'logged out failed!'
        productStore.setNotification(type , message)
      })
      return Promise.reject(e)
    })
  }
}

decorate(AuthStore, {
  user_auth_cookie: observable,
  user_auth_token: observable,
  username: observable,
  password: observable,
  setUsername: action.bound,
  setPassword: action.bound,
  login: action.bound,
  logout: action.bound,
  getUserCookie: computed,
  setCookie: action.bound,
  fetchUserAuthToken: action.bound
});

const authStore = new AuthStore()
window.auth = authStore
export default authStore
