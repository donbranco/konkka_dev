import { observable, action, decorate } from 'mobx'
import authStore from './authStore'

// import langauge catagories
import catalogNL from '../locales/nl/messages.js'
import catalogEN from '../locales/en/messages.js'
import catalogFI from '../locales/fi/messages.js'


class CommonStore {

  appName = 'Konkurssihuutokauppa'
  appLoaded = false
  languageSetting = 'fi'
  catalog = { fi: catalogFI }

  setLanguageSetting = lang => {
    console.log('changing language -- maybe run in action')
    
    switch (lang) {
      case 'fi':
        this.languageSetting = lang
        this.catalog = { fi: catalogFI }
        break;
    
      case 'en':
        this.languageSetting = lang
        this.catalog = { en: catalogEN}
        break;
    
      case 'nl':
        this.languageSetting = lang
        this.catalog = { nl: catalogNL }
        break;
    
      default:
        break;
    }





  }

  getUserID = async () => {
    const cookie = await this.getHeaderToken()
    if (cookie) return cookie.uid
    return false
  }

  getSessionID = async () => {
    const cookie = await this.getHeaderToken()
    if(cookie) return cookie.sessionid
    return null
  }

  getToken = async () => {
    const cookie = await this.getHeaderToken()
    if(cookie) return cookie.token
    return null
  }

  getHeaderToken = () => {
    const cookie = authStore.user_auth_cookie 
    if(typeof cookie.value !== 'undefined') return cookie.value
    return null
  }

  setToken = (token) => this.token = token

  setAppLoaded = () => this.appLoaded = true 
}

decorate(CommonStore, {
  appName: observable,
  token: observable,
  appLoaded: observable,
  languageSetting: observable,
  catalog: observable,
  getSessionId: action.bound,
  setToken: action.bound,
  setAppLoaded: action.bound,
  setLanguageSetting: action.bound,
});

const commonStore = new CommonStore()
window.common = commonStore
export default commonStore
