import { observable, action, decorate, configure, runInAction, autorun } from 'mobx'

import productStore from './productStore'

import api from '../api'

configure({ enforceActions: 'observed' })

class LayoutStore {
  // check wether something is mobile and if the layout
  // of the pages need to be changed

  categories = false
  locations = false
  cities = false
  loaded = false
  screenWidth = null
  staticPages = null
  totalActiveFilters = null

  trackScreenWidth = () => {
    autorun(() => {
      this.screenWidth = window.innerWidth
    })
  }

  setStaticPages = pages => this.staticPages = pages

  setFilterCounter = ( total_selected_filters = 0 ) => {
    const { filterAnyPrice, filterTag, filterAuctionType, filterLocation, filterText, filterStartingPrice, filterEndingPrice, filterSubCategory } = productStore


    console.log('locatino: ', filterLocation)
    
 
    // Filters
    if(filterAnyPrice !== '0') total_selected_filters += 1
    if(filterTag !== null) total_selected_filters += 1
    if(filterSubCategory !== false) total_selected_filters += 1
    if(filterAuctionType !== 0) total_selected_filters += 1
    if(filterLocation !== null && filterLocation !== '') total_selected_filters += 1
    if(filterText !== '') total_selected_filters += 1
    if(filterStartingPrice !== 0) total_selected_filters += 1
    if(filterEndingPrice !== 'No limit') total_selected_filters += 1

    console.log('set active filters: ', total_selected_filters)

    this.totalActiveFilters = total_selected_filters
  }

  getTags = async () => {
    return api.Tags.listTags()
      .then((response) => {
        runInAction(() => {
          this.categories = response.data.tags
          this.loaded = true
        })
        return response.data.tags
      })
      .catch((err) => {
        console.log(err)
      })
  }

  getLocations = async () => {
    return api.Tags.locations()
      .then((response) => {
        runInAction(() => {
          this.getCities(response.data)
          this.locations = response.data
          this.loaded = true
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }

  getCities = (locations) => {
    if (locations) {
      const _locations = locations.locations
      const _cities = Object.keys(_locations).map(function (city) {
        return { city: city, hits: _locations[city] }
      })

      runInAction(() => {
        this.cities = _cities
      })
    }
  }
}

decorate(LayoutStore, {
  // side_menu: observable,
  // toggleMenu: action,

  staticPages: observable,
  screenWidth: observable,
  catogeries: observable,
  cities: observable,
  locations: observable,
  loaded: observable,
  totalActiveFilters: observable,
  getCategories: action,
  getTags: action,
  getLocations: action,
  getCities: action,
  setStaticPages: action,
  setFilterCounter: action,
  trackScreenWidth: action,
})

const layoutStore = new LayoutStore()
window.layout = layoutStore
export default layoutStore
