import { observable, action, decorate, configure, runInAction } from 'mobx'
import io from 'socket.io-client'
// import moment from 'moment'

import routes from '../routes/routes'
import api from '../api'

configure({ enforceActions: 'observed' })
class ProductStore {
  constructor() {
    // setup socket connection through mobx
    this.socket = io(routes.NODE_ROOT_HTTPS)

    // * listen to socket events and execute functions based on those socket events
    this.socket.on('RECEIVE_BID', (data) => {
      console.log('RECEIVE_BID --- productstore : ', data)
      // update single product page
      // this.updateHighestBid(data.bids.bid_amount)
      // this.updateAuctionEnding(data.auction_ends)
      // this.updateProductDataList(data)
    })

    this.socket.on('RECEIVE_BID_IS_SET', state => {
      console.log('set update react')
      // this.socketOnBidsIsPlaced = !this.socketOnBidsIsPlaced
      this.updatesocketOnBidsIsPlaced(state)
    })

    this.socket.on('BID_BEEN_PLACED', state => {
      console.log('set reload false')
      // this.socketOnBidsIsPlaced = state
      this.updatesocketOnBidsIsPlaced(state)
    })

    this.socket.on('BID_IS_REMOVED', () => {
      // update bid list
      // this.loadBidData()
      console.log('remove bid')
    })
  }

  // Initial list, loaded on first app load
  productIdList = observable.map()
  totalProducts = false
  productIdsLoaded = false
  socketOnBidsIsPlaced = true

  //  Detailed product list, loaded based on filter
  productData = null
  productDataLoaded = false

  // disable filters while searching
  disableFilters = false

  // Filters
  filterAnyPrice = '0'
  filterTag = null
  // filterTagSubCategories = false
  filterAuctionType = 0
  filterLocation = null
  filterText = ''
  filterStartingPrice = 0
  filterEndingPrice = 'No limit'
  filterSortBy = 'ending time'
  filterSortDirection = 'ASC'
  filterStatus = 'active'
  filterSubCategory = false

  // single product page
  placed_bids = []
  currentHighestBid = 0
  currentHighestBid_ts = 0
  currentAuctionEnding = 0
  fullProductAddress = 0
  contactPersons = []
  timer = 0
  pid = false
  interval = false
  isLoading = false

  error = false
  errorMessage = ''

  // default page settings
  totalPagesCount = 0
  currentPage = 0
  limit = 18

  // notification settings
  notificationAlert = false
  notificationType = false
  notificationMessage = false

  updatesocketOnBidsIsPlaced = state => this.socketOnBidsIsPlaced = state

  // set product pid on single page
  setPid = (pid) => this.pid = pid

  // clear pid single page on unmount
  clearPid = () => this.pid = false

  // clear bidlist on page unmount
  clearBidList = () => this.placed_bids = []

  // * clear bid obervables and set new data
  clearBidData = () => {
    this.currentAuctionEnding = false
    this.currentHighestBid = 0
    this.clearCounter()
  }

  // * single product page
  updateHighestBid = bid => this.currentHighestBid = bid > this.currentHighestBid ? bid : this.currentHighestBid

  setProductAddress = address => {
    this.fullProductAddress = {
      address: address.thoroughfare,
      city: address.locality,
      postal_code: address.postal_code
    }
  }

  setContactPersons = contacts => this.contactPersons = contacts

  // ! probably not going to need them
  clearSingleProductPage = () => {
    this.currentHighestBid = false
    this.currentHighestBid_ts = false
    this.currentAuctionEnding = false
    this.fullProductAddress = false
    this.contactPersons = []
  }

  setPage = page => this.currentPage = page

  calcTotalPages = () => {
    return Math.ceil(this.productIdList.length / this.limit)
  }

  setTotalPages = pages => runInAction(() =>  this.totalPagesCount = pages )

  clear = () => this.productData.clear()

  clearPagination = () => this.currentPage = 0

  clearTotalProducts = () => this.totalProducts = ''

  // * Functionalities below are for product list

  // Helper function to remove index from an array
  removeIndex = products => {
    const new_array = products.map((product) => {
      return product.id
    })
    return new_array
  }

  // * Helper function to convert array to JSON
  convertDataToJSON = (products) => {
    if (products) {
      // ? maybe switch into a map()
      let arrayList = []
      for (let index in products) {
        try {
          arrayList.push(JSON.parse(products[index]))
        } catch (e) {
          arrayList.push(products[index])
        }
      }
      return arrayList
    } else {
      return products
    }
  }

  setLimit = pids => {
    const init = parseInt(this.currentPage) * parseInt(this.limit)
    return pids.slice(init, init + this.limit)
  }

  getCurrentPageProductIdList = () => {
    const init = parseInt(this.currentPage) * parseInt(this.limit)
    const product_ids = this.productIdList.slice(init, init + this.limit)
    return product_ids
  }

  // advanced filters set and get
  searchFilterData = async (filter, input) => {
    await this.setFilterData(filter, input)
    this.getProductDetailList('advancedFilter')
  }

  // set advanced filters
  setFilterData = (filter, input) => {
    this.disableFilters = true

    switch (filter) {
      case 'category':
        // set sub by default to false in order to clear the search body
        this.filterSubCategory = false

        // set search body
        if (input === 'All') return this.filterTag = null
        this.filterTag = input
        break


      case 'subcategory':
        if (input === 'false') return this.filterSubCategory = false
        this.filterSubCategory = input
        break


      case 'location':
        this.filterLocation = input === "false" ? false : input
        break


      case 'search-text':
        this.filterText = input
        break


      case 'min-price':
        const type = typeof this.filterEndingPrice !== 'string'
        const max_price = parseInt(this.filterEndingPrice)
        const min_price = parseInt(this.filterStartingPrice)
        const price = parseInt(input)

        // option shouldn't work when start is bigger than end
        if (parseInt(input) >= max_price && type) return min_price
        this.filterStartingPrice = price
        break

      case 'max-price':
        if (input === 'No limit') {
          this.filterEndingPrice = 'No limit'
        } else {
          const max_price2 = parseInt(this.filterEndingPrice)
          const min_price2 = parseInt(this.filterStartingPrice)
          const price2 = parseInt(input)

          // option shouldn't work when start is bigger than end
          if (min_price2 >= parseInt(input)) return max_price2
          this.filterEndingPrice = price2
        }
        break


      case 'auction-types':
        this.filterAuctionType = parseInt(input)
        break


      case 'auction-status':
        this.filterStatus = input.toLowerCase()
        break


      case 'sort-by':
        this.filterSortBy = input.sort
        this.filterSortDirection = input.direction
        break


      case 'any_price':
        if (this.filterAnyPrice === '1') return this.filterAnyPrice = '0'
        this.filterAnyPrice = '1'
        break


      case 'reset-filters':
        // Filters
        this.filterAnyPrice = '0'
        this.filterTag = null
        this.filterTagSubCategories = false
        this.filterAuctionType = 0
        this.filterLocation = null
        this.filterText = ''
        this.filterStartingPrice = 0
        this.filterEndingPrice = 'No limit'
        this.filterSortBy = 'ending time'
        this.filterSortDirection = 'ASC'
        this.filterStatus = 'active'
        this.filterSubCategory = false
        this.disableFilters = false
        this.getProductDetailList()
        break

      default:
        console.log('none of the fitler cases worked.')
        break
    }
  }

  // !! try catch request with when id comes from druopal and checks with  node


  // * get product details based on filter type
  // * and returns the product data
  getProductDetailList = async () => {
    this.errorMessage = false
    this.isLoading = true

    const filters = await this.setFilterBody()
    console.log('Filter options: ', filters)

    // get product ids based on filters
    const response = await api.Filter.getByAdvandedFilter(filters)
    const products_ids = response.data.products
    console.log('products after the filter --- ', products_ids);

    // canncel the function of there are no product_ids
    if(products_ids <= 0) return this.cancelProductRequest()

    // store all product ids from results
    this.setTotalProducts(products_ids)

    // set limit for sending maximum amount of products based on current page
    const pids = await this.setLimit(await this.removeIndex([...products_ids]))
    const productData = await api.Products.productDetails({ product_ids: pids })

    console.log('product data: ', productData)

    // check if data exists
    if (typeof productData !== 'undefined' && productData.length !== 0) {
      const check_response_format = this.convertDataToJSON(productData.data.products)
      const includingProductData = await this.checkWithNode(check_response_format)
      
      runInAction(() => {
        this.productData = includingProductData
        this.productDataLoaded = true
        this.isLoading = false
        this.totalPagesCount = Math.ceil(this.productIdList.length / this.limit)
        this.disableFilters = false
      })
      return true
    } else {
      runInAction(() => {
        this.productData = false
        this.disableFilters = false
        this.errorHandler('filter_response')
      })
      return true
    }
  } // end getProductDetailList

  cancelProductRequest = () => {
    runInAction(() => {
      this.productData = false
      this.disableFilters = false
      this.errorHandler('filter_response')
    })
  }

  updateProductDataList = data => {
    const new_product_data = this.productData.map(product => {
      if(product.id === data.pid) return this.updateProductDataAfterBid({ product, data })
      return product
    })
    runInAction(() => {
      this.productData = new_product_data
    })
  }

  updateProductDataAfterBid = ({ product, data }) => {
    product.price.commerce_price = parseInt(data.bids.bid_amount).toFixed(2)
    product.ending_time = data.auction_ends
    return product
  }

  // set the filter in the body
  setFilterBody = () => {
    // * set all the filter parameters
    const _type = this.filterAuctionType ? this.filterAuctionType : null
    const _search_text = this.filterText ? this.filterText : null
    const _location = this.filterLocation !== null ? [this.filterLocation] : null

    const _sort = this.filterSortBy ? this.filterSortBy : null
    const _direction = this.filterSortDirection ? this.filterSortDirection : null

    const _price_min = this.filterStartingPrice * 100
    const _price_max = this.filterEndingPrice === 'No limit' ? 0 : this.filterEndingPrice * 100

    const _tags = this.filterSubCategory ? [this.filterSubCategory] : [this.filterTag]

    // * set filter options in a body
    return {
      auction_status: this.filterStatus,
      type: _type,
      text: _search_text,
      tags: _tags,
      locations: _location,
      sort_by: _sort,
      sort_direction: _direction,
      price_min: _price_min,
      price_max: _price_max,
      any_price_accepted: this.filterAnyPrice,
    }
  }

  setTotalProducts = pids => {
    // check if there are any products with search result
    if (pids.length > 0) {
      runInAction(() => {
        this.productIdList = pids
        this.totalProducts = pids.length
        this.totalPagesCount = pids.length / this.limit
      })
    } else {
      // no products were found with those search filters
      runInAction(() => {
        this.totalProducts = 0
        this.productIdList = false
        this.productData = false
        this.errorMessage = 'No products were found'
      })
    }
  }

  // * check if bids are placed and get latest data from node
  checkWithNode = async products => {
    console.log('check with node: ', products)
    return await Promise.all(
      products.map(async (product) => {
        const res = await api.Bids.listBidsByHighest(product.id)
        if (res.data.length > 0) return await this.updateProductData(product, res.data[0])
        return product
      })
    )
  }

  // * update product data if auction is active on Node
  updateProductData = (product, auction) => {
    console.log('auction: ', auction)
    product.price.commerce_price = auction.bids.bid_amount
    product.ending_time = auction.auction_ends
    return product
  }


  // helper functions to load bid list in component Card
  getStartBidFromNode = async pid => {
    return await api.Bids.listBidsByHighest(pid)
      .then(response => { return response.data})
      .catch(e => console.log(e))
  }

    // helper functions to load bid list in component Card
  getStartBidFromDrupal = pid => {
    const body = { product_ids: [`${pid}`] }
    api.Products.productDetails(body)
      .then(response => { return JSON.parse(response.data.products[pid]) })
      .catch(e => console.error(e))
  }

  // setStartBidFromNode = (bids) => {
  //   runInAction(() => this.placed_bids = bids)
  //   this.setFromNodeCurrentProduct(bids[0])
  // }

  // setStartBidFromDrupal = (data) => this.setFromDrupalCurrentProduct(data)

  // ! might be removed if socket remove works
  // removeBid
  deleteBidFromList = (pid, ts) => {
    const bid_data = { pid, ts }
    this.socket.emit('REMOVE_BID', bid_data)
  }

  // TODO: end ----

  // * single product page with node (when there are bids) -- also sets aution ending
  setFromNodeCurrentProduct = data => {
    this.updateHighestBid(data.bids.bid_amount)
    // this.updateAuctionEnding(data.auction_ends)
    this.setCurrentAuctionEndingTime(data.auction_ends)
  }

  // * single product page with drupal (init) -- also sets aution ending
  setFromDrupalCurrentProduct = data => {
    const price = data.price.commerce_price ? data.price.commerce_price : data.price.starting_price
    this.updateHighestBid(price)
    // this.updateAuctionEnding(data.ending_time)
    this.setCurrentAuctionEndingTime(data.ending_time)
  }

  // * init single product page data
  setProductPage = data => {
    this.setContactPersons(data.contacts)
    this.setProductAddress(data.address)
  }

  // * update observable ending time
  setCurrentAuctionEndingTime = ts => {
    if (this.currentAuctionEnding === '' || ts > this.currentAuctionEnding) this.currentAuctionEnding = ts
  }

  // * send bid
  sendBid = data => this.socket.emit('SEND_BID', data)
  
  // * send proxy bid
  addProxyBid = data => this.socket.emit('ADD_PROXY', data)

  // * set observables in order to display notification
  setNotification = (type, message) => {
    this.notificationAlert = true
    this.notificationType = type
    this.notificationMessage = message

    // clear obersavbles once notifcation is shown
    // setTimeout(() => this.clearNotification(), 3000)
  }

  // * clear notification observables for next notifcation
  clearNotification = () => {
    this.notificationAlert = false
    this.notificationType = false
    this.notificationMessage = false
  }

  // handle different error message
  errorHandler = type => {
    switch (type) {
      case 'filter_response':
        this.errorMessage = 'No search results were found'
        break

      case 'something else':
        this.errorMessage = 'No search results were found'
        break

      case 'something else 2':
        this.errorMessage = 'No search results were found'
        break

      default:
        this.errorMessage = 'No fucking clue'
        break
    }
  }
}

decorate(ProductStore, {
  // TODO: maybe define internal observables with an underscore
  // auctionEnds: observable,
  disableFilters: observable,
  placed_bids: observable,
  contactPersons: observable,
  currentAuctionEnding: observable,
  currentHighestBid: observable,
  currentProduct: observable,
  errorMessage: observable,
  filterAuctionType: observable,
  filterSortby: observable,
  filterSortDirection: observable,
  filterStatus: observable,
  filterEndingPrice: observable,
  filterEndingPriceWithComma: observable,
  filterTag: observable,
  // filterTagSubCategories: observable,
  filterAnyPrice: observable,
  filterLocation: observable,
  filterStartingPrice: observable,
  filterStartingPriceWithComma: observable,
  filterSubCategory: observable,
  filterText: observable,


  fullProductAddress: observable,
  limit: observable,
  notificationAlert: observable,
  notificationType: observable,
  notificationMessage: observable,
  productIdsLoaded: observable,
  productDataLoaded: observable,
  pid: observable,
  currentPage: observable,
  totalProducts: observable,
  timer: observable,
  isLoading: observable,
  interval: observable,
  socketOnBidsIsPlaced: observable,

  // mostly internal functions
  // TODO: maybe define internal functions with an underscore
  calcTimeDifference: action,
  calcTotalPages: action,
  clearBidData: action,
  clearBidList: action,
  clearPagination: action,
  clearPid: action,
  clearTotalProducts: action,
  clearCounter: action,
  clearSingleProductPage: action,
  clearNotification: action,
  checkWithNode: action.bound,
  checkIfProductIsShown: action.bound,
  countDown: action,

  // error handler
  errorHandler: action,

  // get data
  getCurrentPageProductIdList: action,
  getProductIdList: action,
  getProductDetailList: action,
  getSelectedCategory: action,
  getStartBidFromNode: action,
  getStartBidFromDrupal: action,

  initFilterTag: action,

  loadBidData: action,

  // set data
  setAuctionEnding: action,
  sendBid: action.bound,
  setContactPersons: action,
  setCurrentProduct: action,
  setFromNodeCurrentProduct: action,
  setFromDrupalCurrentProduct: action,
  setFilterData: action,
  searchFilterData: action.bound,
  setPage: action,
  setPid: action,
  setProductAddress: action,
  setNotification: action,
  setStartBidFromNode: action,
  setStartBidFromDrupal: action,
  setTotalPages: action,
  setTotalProducts: action,

  // update data
  // updateAuctionEnding: action,
  updateCounter: action,
  updateHighestBid: action,
  updateProductData: action,
  updateProductDataList: action,
  updateProductDataAfterBid: action,
  updateProduct: action,
})

const productStore = new ProductStore()
window.store = productStore
export default productStore
