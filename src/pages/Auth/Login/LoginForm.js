import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';

import './Login.css';

class LoginForm extends Component {
  handleUsernameChange = (e) => {
    e.preventDefault();
    this.props.authStore.setUsername(e.target.value);
  }

  handlePasswordChange = (e) => {
    e.preventDefault();
    this.props.authStore.setPassword(e.target.value);
  }

  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.authStore.login();
  };

  render() {
    const { username, password } = this.props.authStore;  

    return (
      <>
        <Col md={{ size: 12 }} className="login-form">
          <Form onSubmit={this.handleSubmitForm}>
            <FormGroup row>
              <Col sm={12}>
                <Input
                  type="text"
                  onChange={this.handleUsernameChange}
                  value={username}
                  name="text"
                  id="exampleEmail"
                  placeholder="sähköpostiosoite"
                  className="login-form__input"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12}>
                <Input type="password" onChange={this.handlePasswordChange} value={password} name="password" id="examplePassword" placeholder="salasana" className="login-form__input" />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12} md={{ size: 12 }} className="login-form__button padding-30" >
                <Button
                  type="submit" className="login-form__button-login"
                >Kirjaudu sisään</Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={6} md={{ size: 6 }} className="login-form__button login-form__flex-remember" >
                <Input addon type="checkbox" aria-label="Checkbox for remember-me" />
                <Label>Muista minut</Label>
              </Col>
              <Col sm={6} md={{ size: 6 }} className="login-form__button no-padding" >
                <Label className="login-form__button-forgot">Unohtuiko salasana?</Label>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </>
    );
  }
}

export default inject('authStore')(observer(LoginForm));