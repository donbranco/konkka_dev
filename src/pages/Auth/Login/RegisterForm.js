import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Col, Form, FormGroup } from 'reactstrap';

import './Login.css';

class RegisterForm extends Component {
  handleUsernameChange = (e) => {
    this.props.authStore.setUsername(e.target.value);
  }

  handlePasswordChange = (e) => {
    this.props.authStore.setPassword(e.target.value);
  }

  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.authStore.register();
  };

  render() {
    // const { username, password } = this.props.authStore;

    return (
      <>
        <Col md={{ size: 12 }} className="login-form">
          <Form onSubmit={this.handleSubmitForm}>
            <FormGroup row>
              <Col xs={12}>
                <a href="#">Register from drupal</a>
              </Col>
              {/* <Col sm={12}>
                <Input
                  type="text"
                  onChange={this.handleUsernameChange}
                  value={username}
                  name="text"
                  id="exampleEmail"
                  placeholder="sähköpostiosoite"
                  className="login-form__input"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12}>
                <Input type="password" onChange={this.handlePasswordChange} value={password} name="password" id="examplePassword" placeholder="salasana" className="login-form__input" />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12} md={{ size: 12 }} className="login-form__button padding-30" >
                <Button
                  type="submit" className="login-form__button-login"
                >Rekisteröidy käyttäjäksi</Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12} md={{ size: 12 }} className="login-form__button login-form__flex-remember" >
                <Input addon type="checkbox" aria-label="Checkbox for remember-me" />
                <Label>Hyväksyn käyttöehdotminut</Label>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12} md={{ size: 12 }} className="login-form__button fb" >
                <Button className="login-form__button-fb" >Kirjaudu sisään palvelulla Facebook</Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12} md={{ size: 12 }} className="login-form__button google" >
                <Button>Kirjaudu sisään palvelulla Google</Button>
              </Col> */}
            </FormGroup>
          </Form>
        </Col>
      </>
    );
  }
}

export default inject('authStore')(observer(RegisterForm));