import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Col, Button, Form, FormGroup, Label, Input, Container, Row } from 'reactstrap';

import Aux from '../../../hoc/Aux/Aux';
import Loader from '../../../components/UI/Loader/Loader';

import './Login.css';

class Login extends Component {
	state = {
		isLoading: false
	};

	handleUsernameChange = (e) => {
		this.props.authStore.setUsername(e.target.value);
	};

	handlePasswordChange = (e) => {
		this.props.authStore.setPassword(e.target.value);
	};

	handleSubmitForm = (e) => {
		e.preventDefault();

		// ? why the two loading screens?
		this.setState({ isLoading: true });
		this.props.authStore.login();
		this.setState({ isLoading: false });
	};

	render() {
		const { username, password } = this.props.authStore;

		var loginForm = null;

		if (!this.state.isLoading) {
			loginForm = (
				<Aux>
					<Container fluid>
						<Row>
							<Col md={{ size: 8, offset: 2 }}>
								<h1>Kirjaudu sisään</h1>
							</Col>
						</Row>
					</Container>
					<Container fluid>
						<Row>
							<Col md={{ size: 8, offset: 2 }}>
								<Form onSubmit={this.handleSubmitForm}>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											Sähköposti
										</Label>
										<Col sm={10}>
											<Input
												type="text"
												onChange={this.handleUsernameChange}
												value={username}
												name="text"
												id="exampleEmail"
												placeholder="Syötä sähköposti"
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											Salasana
										</Label>
										<Col sm={10}>
											<Input
												type="password"
												onChange={this.handlePasswordChange}
												value={password}
												name="password"
												id="examplePassword"
												placeholder="Syötä salasana"
											/>
										</Col>
									</FormGroup>
									<FormGroup check row>
										<Col md={{ size: 12 }}>
											<Button type="submit">Kirjaudu</Button>
										</Col>
									</FormGroup>
								</Form>
							</Col>
						</Row>
					</Container>
				</Aux>
			);
		} else {
			loginForm = <Loader />;
		}

		return <Aux>{loginForm}</Aux>;
	}
}

export default inject('authStore')(observer(Login));
