import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Col } from 'reactstrap'

// Custom components
import GenerateCards from './GenerateCards'
import Loader from '../../components/UI/Loader/Loader'

class ProductCard extends Component {

	render() {
		const { isLoading, errorMessage, productData } = this.props.productStore

		// check if there are any errors before creating the cards
		if (!isLoading && !errorMessage && productData) {
			return <GenerateCards />
	  } else if (productData === false && errorMessage) {
			return <Col col={12}><h2>No results with your current search filters.</h2></Col>
		}else {
			return <Loader />
		}
	}
}

export default inject('productStore')(observer(ProductCard))