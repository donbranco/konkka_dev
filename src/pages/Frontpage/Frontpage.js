import React, { Component } from 'react';
import { Container, Row } from 'reactstrap';

import Products from './Products';
import Hero from '../../components/Hero/Hero';
import Aux from '../../hoc/Aux/Aux';

import './Frontpage.css';

export default class Frontpage extends Component {
	render() {
		return (
			<Aux>
				<Hero
					title="Suomen mielenkiintoisin ja monipuolisin huutokauppa"
					paragraph="Konkka v3 sivut avattu 1.1.2019"
				/>
				<Container fluid>
					<Row>
						<Products />
					</Row>
				</Container>
			</Aux>
		);
	}
}
