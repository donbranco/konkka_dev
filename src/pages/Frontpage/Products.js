import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'

import { Trans } from '@lingui/macro'

// Custom components
import SortBy from '../Products/Product_components/SortBy'
import ProductCard from './ProductCard'
import ListPagination from '../../components/UI/ListPagination/ListPagination'

class Products extends Component {

	// clear notifaction when loading a new view so that old notifcation will not be shown
	componentWillUnmount = async () =>  {
		this.props.productStore.clearNotification()
	}

	// pagination for getting and setting product data
	// ? maybe async if it causes problems
	handleSetPage = page => {
		this.props.productStore.setPage(page)
		this.props.productStore.getProductDetailList()
	}

	// submit filter/order direction
	filterSubmission = e => {
		const filter = e.currentTarget

		if(filter.name === 'sort-by') {
			// set order direction
			const data = {
				direction: filter.getAttribute("data-direction"),
				sort: filter.getAttribute("data-sort"),
			}
			// set order direction
			this.props.productStore.setFilterData(filter.name, data)
		} else {
			// set other filters	
			this.props.productStore.setFilterData(filter.name, filter.value)
		}
		// retrieve new product ids with filter input and clearing pagination
		this.props.productStore.clearPagination()
		this.props.productStore.getProductDetailList('advancedFilter')
  }

	render() {
		const { limit, totalProducts, currentPage, isLoading } = this.props.productStore
		const total_products = !isLoading ? totalProducts : ''
		let total_pages = Math.ceil(totalProducts / limit)

		return (
			<Container fluid>
				<Row className='filter__wrapper'> 
					<Col xs={4}><label className="filter__title-total col">{`${total_products} `}<Trans>Kohdetta</Trans></label></Col>
					<Col xs={4} className="filter__sort">
						<SortBy filterSubmission={this.filterSubmission} />
					</Col>
				</Row>
				<Row>
					<ProductCard />
				</Row>
				<Row>
					<Col className="pagination pagination__wrapper">
						<ListPagination
							totalPagesCount={total_pages}
							currentPage={currentPage}
							onSetPage={this.handleSetPage}
						/>
					</Col>
				</Row>
			</Container>
		)
	}
}

export default inject('productStore')(withRouter(observer(Products)))
