import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import { Col } from 'reactstrap'

import Card from '../../components/Card/Card'

class GenerateCards extends Component {
  productSelectedHandler = (id) => {
		this.props.history.push({ pathname: '/product/' + id })
	}

  render() {
    // get an array of pid's based on the all pid's where the pagination and and limit
	  // defined which pid's data will be requested from Drupal
		const { getCurrentPageProductIdList, productData } = this.props.productStore
    let product_order = getCurrentPageProductIdList()

    return product_order.map(order => {
      // return <ProductCards order={order} products={productData} history={this.props.history} key={i} />
      return productData.map((product, i) => {
        if(parseInt(parseInt(order.id)) === parseInt(product.id)) {
          const { title, short_description, ending_time, id } = product;
          const { commerce_price, starting_price, any_price_accepted } = product.price
          const price = commerce_price ? commerce_price : starting_price
          let image
  
          // check if there are any images set, if not set the default
          try {
            image = product.images[0].thumbnail_url
          } catch (e) {
            image =
              'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjRu9DEmcXiAhWIw8QBHROIAk8QjRx6BAgBEAU&url=https%3A%2F%2Fdrinkotron.com%2Fmemes&psig=AOvVaw0N4nZRM6F6l3ZGDT7KMXdj&ust=1559372471178919'
          }
  
          // create a product card
          return (
            <Col key={i} md="4">
              <Card
                pid={id}
                title={title}
                description={short_description}
                imageUrl={image}
                price={price}
                any_price_accepted={any_price_accepted}
                city={product.address.locality}
                timeLeft={ending_time}
                clicked={() => this.props.history.push({ pathname: '/product/' + id })}
              />
            </Col>
          )
        }
        return <></>
      })
    })
	}
}

export default inject('productStore')(withRouter(observer(GenerateCards, Card)))


// const ProductCards = ({ order, products, history}) => {
//     let cards = products.map((product, i) => {
//       if(parseInt(order) === parseInt(product.id)) {
//         const { title, short_description, ending_time, id } = product;
//         const { commerce_price, starting_price, any_price_accepted } = product.price
//         const price = commerce_price ? commerce_price : starting_price
//         let image

//         // check if there are any images set, if not set the default
//         try {
//           image = product.images[0].thumbnail_url
//         } catch (e) {
//           image =
//             'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjRu9DEmcXiAhWIw8QBHROIAk8QjRx6BAgBEAU&url=https%3A%2F%2Fdrinkotron.com%2Fmemes&psig=AOvVaw0N4nZRM6F6l3ZGDT7KMXdj&ust=1559372471178919'
//         }

//         // create a product card
//         return (
//             <Col key={i} md="4">
//               <Card
//                 pid={id}
//                 title={title}
//                 description={short_description}
//                 imageUrl={image}
//                 price={price}
//                 any_price_accepted={any_price_accepted}
//                 city={product.address.locality}
//                 timeLeft={ending_time}
//                 clicked={() => history.push({ pathname: '/product/' + id })}
//               />
//             </Col>
//         )
//       } 
//       return null
//     })
//     return cards
// }
