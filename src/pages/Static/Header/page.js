import React from 'react'
import ReactHtmlParser from 'react-html-parser';
import { observer, inject } from 'mobx-react'
import { Row, Container, Col } from 'reactstrap'

function page(props) {
  const nid = props.match.params.id
  const data = props.layoutStore.staticPages.filter(page => page.nid === nid)
  // Changed, images, lang, new_content, status, <--- destructure next line
  const { author_uid, body, created, title } = data[0]

  return (
    <Container>
      <Row>
        <Col xs={12}>
          <div>
            <h1>{ title }</h1>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <div>
            <p>{`${author_uid}, ${created}`}</p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <div>
           { ReactHtmlParser(body) }
          </div>
        </Col>
      </Row>
    </Container>
  )
}


export default inject('layoutStore')(observer(page))