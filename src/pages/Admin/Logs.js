import React, { Component } from 'react'
import { Container, Row, Col, Form, ListGroup, ListGroupItem, Button, Input } from 'reactstrap'
import io from 'socket.io-client'


import './admin.css'
import routes from '../../routes/routes'
import api from '../../api'

// import log from '../../logs/log_events'
// var postLogEvent = require('../../logs/log_events').postLogEvent;


export default class Logs extends Component {
  constructor(props) {
    super(props)

    this.socket = io(routes.NODE_ROOT_HTTPS)

    this.state = {
      LogType: null,
      LogList: null,
      SearchType: ''
    }

    this.socket.on('UPDATE_LOG', data => {
      console.log('update log view')
      api.Log.getAllLogs()
      .then(logs => this.setState({ LogList: logs.data}))
      .catch(e => console.log(e))
    })
  }

  componentWillMount = () => {
    api.Log.getAllLogs()
    .then(logs => this.setState({ LogList: logs.data}))
    .catch(e => console.log(e))
  }

  handleInputChange = (e) => {
		const state = e.currentTarget.name
		const value = e.currentTarget.value
		this.setState({ [state]: value })
  }
  
  getLogsWithType = (e) => {
    e.preventDefault()

    if(this.state.SearchType !== '') {
      api.Log.getLogByType(this.state.SearchType)
      .then(logs => this.setState({ LogList: logs.data}))
      .catch(e => console.log(e))
    } else {
      api.Log.getAllLogs()
      .then(logs => this.setState({ LogList: logs.data}))
      .catch(e => console.log(e))
    }
  }

  sendTestLog = (e) => {
    e.preventDefault()

    const data = {
      "type": 1,
      "data": {
        "uid": 129,
        "pid": 124,
        "action": "test action",
        "message": "This message is an error"
      },
      "timestamp": 1565178682252
    }

  api.Log.postNewLog(data)

  }

  render() {
    const { SearchType, LogList } = this.state

    const SearchButton = (
			<Row style={{ justifyContent: 'flex-start', alignContent: 'center', alignItems: 'center' }}>
        <Form className='log-submit-form' onSubmit={this.getLogsWithType}>
          <Col sm={8}>
            <Input
              name="SearchType"
              value={SearchType}
              style={{maxWidth: 250 }}
              className='log-search-input'
              placeholder='1: success, 2: warnings, 3: errors'
              onChange={this.handleInputChange}
            />
          </Col>
          <Col sm={4}>
            <Button 
              className='log-search-button'
              onSubmit={(() => this.getLogsWithType)}>
              Search log type
            </Button>
          </Col>
        </Form>
      </Row>
		)

    return (
      <Container>
        <Row className='log-header'>
          <Col sm={4}>
            <h1>Logs</h1>
          </Col>
          <Col sm={4}>
            { SearchButton }
          </Col>
          <Button onClick={this.sendTestLog}>post test log</Button>
        </Row>
        <Row>
          <Col sm={12} className='log log-table'>
            <GenerateLogs logs={LogList} />
          </Col>
        </Row>
      </Container>
    )
  }
}


export function GenerateLogs({ logs }) {
  if(logs !== null) {
    return logs.map((log, i) => {
      const { type, data, timestamp } = log
      const { uid, pid, action, message } = data
      let color
      if(type === 1) color = 'success'
      if(type === 2) color = 'warning'
      if(type === 3) color = 'danger'

      // if(type === 1) color = { background: '#00AB66', margin: 15, padding: 10, fontSize: 16 }
      // if(type === 2) color = { background: '#ffae42', margin: 15, padding: 10, fontSize: 16 }
      // if(type === 3) color = { background: '#A63232', margin: 15, padding: 10, fontSize: 16 }
      
      return (
        <ListGroup key={i}>
          <ListGroupItem color={color}>
            <Row>
              <Col sm={1}>{i}</Col>
              <Col sm={1}>{type}</Col>
              <Col sm={1}>{timestamp}</Col>
              <Col sm={1}>{uid}</Col>
              <Col sm={1}>{pid}</Col>
              <Col sm={2}>{action}</Col>
              <Col sm={5}>{message}</Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      )
    })
  }
  return <p>Logs are being loaded</p>
}

