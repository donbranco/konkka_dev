import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Container, Row } from 'reactstrap';
import Helmet from 'react-helmet';

import Aux from '../../hoc/Aux/Aux';

import userStore from '../../stores/userStore';

class Contact extends Component {
  componentDidMount() {
    console.log(this.props)
  }

  render() {
    let content = userStore.currentUser.uid ? ( <h1>You are logged in</h1>) : ( <h1>Please login</h1> )

    return (
      <Aux>
        <Helmet />
        <Container fluid>
          <Row>
            {content}
          </Row>
        </Container>
      </Aux>
    );
  }
}

export default inject('userStore')(observer(Contact));
