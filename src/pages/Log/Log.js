import React from 'react';
// import Axios from 'axios';
import Api from '../../api';
import { ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';

export default class Example extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
  }

  state = {
    logs: null,
    collapse: false,
  }

  toggle() {
    this.setState({collapse: !this.state.collapse})
  }

  componentWillMount() {
    return Api.Logs.listLogs()
    .then(res => {
      this.setState({logs: res.data});
    });
  }

  setType(type) {

    var color = 'success';

    switch(type) {
      case 1: 
      color = 'success';
      break;
      case 2: 
      color = 'warning';
      break;
      case 3:
      color = 'danger';
      break;
      default: color = 'success';
    }
    return color;
  }

  render() {

    if(this.state.logs !== null) {
      return (
        <ListGroup>
          {this.state.logs.map((log, index) => {
            console.log(log);
            return (
              <ListGroupItem color={this.setType(log.type)} key={index} className="justify-content-between">
                <ListGroupItemHeading>{log.data[0].action}</ListGroupItemHeading>
                <ListGroupItemText>{log.data[0].message}</ListGroupItemText>
                <ListGroupItemText>{log.timestamp}</ListGroupItemText>
              </ListGroupItem>
            );
          })}
        </ListGroup>
      );
    } else {
      return <p>Loading logs...</p>;
    }
  }
}