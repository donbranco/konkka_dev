import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Row, Container } from 'reactstrap'
import io from 'socket.io-client'

import api from '../../api'
import routes from '../../routes/routes'

// styles
import './single_page.css'

// Custom components
import Loader from '../../components/UI/Loader/Loader'

import AuctionInfo from './Product_components/AuctionInfo'
import ContactInfo from './Product_components/ContactInfo'
import ProductDetails from './Product_components/ProductDetails'
import ImageSlider from './Product_components/imageSlider'

class Product extends Component {
	constructor(props) {
		super(props)

		this.socket = io(routes.NODE_ROOT_HTTPS)

		this.state = {
			errorMessage: '',
			product: false,
			loading: true,
			bidList: [],
			contacts: [],
			pid: this.props.match.params.id,
			purchased: false,
			bids: [],
			UIDHighestBid: ''
		}

		// update product data if needed
		this.socket.on('RECEIVE_BID', (data) => {
			if(data.pid !== null) {
				if(this.state.pid === data.pid) {
					this.getProductData(data.pid)
					this.loadBidData(data.pid)
				}
			}
		})

		this.socket.on('BID_IS_REMOVED', (pid) => {
			if(this.state.pid === pid) {
				this.getProductData(pid)
				this.loadBidData(pid)
			}
		})
	}

	componentWillMount = () => { 
		this.getProductData()
		this.loadBidData(this.state.pid)
	}

	// clear notifications so that on the new view it will not reload/repeat and old notification
	componentWillUnmount = () => this.props.productStore.clearNotification()

	getProductData = (pid = this.props.match.params.id ) => {
		const body = { product_ids: [ `${pid}` ] }
		let data

		api.Products.productDetails(body)
			.then(response => {
				try {
					data = JSON.parse(response.data.products[pid])
				} catch (e) {
					data = response.data.products[pid]
				}
			})
			// check latest data from drupal
			.then(async () => {
				// const allBids = await api.Bids.listBids(pid)
				const latest_bid = await api.Bids.listBidsByHighest(pid)

				// if data from node is newer update product data before setting state
				if(typeof latest_bid.data !== 'undefined' && latest_bid.data.length > 0) {
					console.log('[BIDS EXIST]')

					const node_bid = latest_bid.data[0]
					const highestBidFromNode = node_bid.bids.bid_amount > 0 ? node_bid.bids.bid_amount : 0
					const UIDHighestBid = node_bid.bids.uid
					const ending_time = node_bid.auction_ends === true ? node_bid.auction_ends : data.ending_time

					// compare node data from drupal to determine the latest data
					if(highestBidFromNode >= data.price.commerce_price ) {
						console.log('[NODE BID IS GEATER THAN CURRENT BID FROM DRUPAL]')
						data.price.commerce_price = highestBidFromNode
						data.ending_time = ending_time
						this.setState({ product: data, loading: false, UIDHighestBid: UIDHighestBid,  pid: pid, purchased: true })	
					} else {
						console.log('[NODE BID IS SMALLER THAN CURRENT BID FROM DRUPAL]')
						this.setState({ product: data, loading: false, pid: pid })
					}
				} else {
					console.log('INIT DATA SINGLE PAGE LOAD: ', data)
					this.setState({ product: data, loading: false, pid: pid })
				}
			})
			.catch(e => console.error('loadbid: ', e))
	}


	 // * loading product data - from node else drupal
  // * controller for loading the data in the  bid list
  loadBidData = async pid => {
    const { getStartBidFromNode, getStartBidFromDrupal } = this.props.productStore
    const node = await getStartBidFromNode(pid)

    console.log('PID to update: ', pid )
    // console.log('Check if it loads from drupal or node: ', node[0].bids.bid_amount );

    // set bidlist with the data from node
    if (typeof node[0] !== 'undefined') return this.setState({ bids: node, highest: node[0].bids.bid_amount })

    // set bidlist with the data from drupal
    // ! probably not needed.....
    const drupal = await getStartBidFromDrupal(pid)
    return this.setState({ bids: drupal })
  }

	render() {
		const { uid } = this.props.userStore.currentUser
		let { product, bidList, loading, error, pid, purchased, bids, UIDHighestBid } = this.state

		const { 
			address,
			// auction_status,
			// buyer_guidance,
			administrators,
			contacts,
			ending_time,
			// id,
			images,
			list,
			// presentations, 
			price,
			// short_description,'
			seller,
			proxy_bidding_allowed,
			text,
			title,
			type,
		} = product


		if (product && !loading) {
			// setup the layout for single product page
			return (
				<Container fluid className="single-product__layout">
					<Row>
						<Container fluid>
							<h1 className="product__title">{title}</h1>
							<Row className="product__container">
								<ImageSlider images={images} />
								<AuctionInfo 
									pid={pid} 
									uid={uid}
									adminUID={administrators}
									type={type.tid}
									bidList={bidList}
									highestBid={price}
									purchased={purchased}
									proxy_bidding={proxy_bidding_allowed}
									time={ending_time} 
									address={address} 
									error={error}
									bids={bids}
									UIDHighestBid={UIDHighestBid}
								/>
							</Row>
							<div className="product__container">
								<ProductDetails description={text} table_content={list} />
								<ContactInfo contacts={contacts} seller={seller} />
							</div>
						</Container>
					</Row>
				</Container>
			)
		}
		return <Loader />
	}
}
export default inject('productStore', 'userStore')(observer(Product))
