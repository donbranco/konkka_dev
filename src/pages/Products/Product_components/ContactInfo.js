import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import {
  ListGroup,
  ListGroupItem,
  // ListGroupItemHeading,
  ListGroupItemText,
  Col
} from 'reactstrap';
import { Trans } from '@lingui/macro'

class ContactInfo extends Component {
  render() {
    // contact details of the seller
    const { contacts, seller } = this.props

    let contact_email, seller_name, seller_address, seller_phone_number, seller_title = <></>


    // !! temp solution while seller is not defined for other products 
    if(typeof seller === 'undefined') return <Trans><p>ei tietoa myyjästä</p></Trans>
    

    const {
      // business_id,
      business_name,
      email,
      name,
      phone_number,
    } =  seller


    // define email-contact button
    if(typeof seller !=='undefined' && typeof email !=='undefined' && email !== '' && email !== null ) {
      contact_email = ( 
        <ListGroupItemText className="product__contacts-link">
         <Trans><a href={`mailto:${email}`}>» Lähetä kysymys</a></Trans>
        </ListGroupItemText>
     )
    }

    // set seller information
    seller_title = business_name !== 'undefined' && business_name !== null ? business_name : name
    if(typeof business_name !== 'undefined' && business_name !== null) seller_name = <ListGroupItemText>{name}</ListGroupItemText>
    if(typeof email !== 'undefined' && email !== null) seller_address = <ListGroupItemText>{email}</ListGroupItemText>
    if(typeof phone_number !== 'undefined' && phone_number !== null) seller_phone_number = <ListGroupItemText>{phone_number}</ListGroupItemText>
   
    return (
      <>
        <Col className="product__contacts wrapper " sm="12" md="4" lg="4">
            <Trans><h3>Kohteen ilmoittaja</h3></Trans>
          <ListGroup className="product__contacts-list">
            <ListGroupItem className="product__contacts seller">
              <h4>{seller_title}</h4>
                {seller_name}
                {seller_address}
                {seller_phone_number}
                {contact_email}
            </ListGroupItem>
          </ListGroup>
          <ListGroup className="product__contacts-list">
            <Trans><h4>muut yhteyshenkilöt</h4></Trans>
            {
              contacts.map((contact, i) => {
                const { name, email, phone } = contact
                let name_field, email_field, phone_field
                if(typeof name !== 'undefined' && name !== null) name_field = <ListGroupItemText>{name}</ListGroupItemText>
                if(typeof email !== 'undefined' && email !== null) email_field = <ListGroupItemText>{email}</ListGroupItemText>
                if(typeof phone !== 'undefined' && phone !== null) phone_field = <ListGroupItemText>{phone}</ListGroupItemText>

                return (
                  <ListGroupItem  key={i} className="product__contacts individual">
                    {name_field}
                    {email_field}
                    {phone_field}
                  </ListGroupItem>
                )
              })
            }
            </ListGroup>
        </Col>
      </>
    )
  }
}

export default inject('productStore')(observer(ContactInfo))
