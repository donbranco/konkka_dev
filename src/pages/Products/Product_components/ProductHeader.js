import React, { PureComponent } from 'react';
import { Container, Row } from 'reactstrap';

export default class ProductHeader extends PureComponent {
	render() {
		console.log('---- Product Header ----');
		return (
			<Container fluid>
				<Row>
					<div className="product__header">
						<img src={this.props.headerImgUrl} alt={this.props.alt} />
					</div>
				</Row>
			</Container>
		);
	}
}
