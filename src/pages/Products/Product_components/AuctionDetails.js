import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import { Trans } from '@lingui/macro'

class AuctionDetails extends Component {
  constructor(props) {
    super(props)

    this.state = {
      counter: this.props.timer,
      days: null,
      hours: null,
      minutes: null,
      seconds: null,
      error: false
    }
  }

  componentDidMount = () => this.setCountDown(this.props.timer)

  componentWillUnmount = () => {
    // maybe unsetting timer if there is a memory leak ??? 
    this.stopCountDown()
    this.setState({
      counter: null,
      days: null,
      hours: null,
      minutes: null,
      seconds: null,
      error: false
    })
  }

  // TODO: set as a seperate component maybe to optimze re-render

  // stop counter --> stopping memory leak
  stopCountDown = () => this.runCountDown({ stop: true })

  setCountDown = (auction_time, duration) => {
    duration = moment.duration(moment(auction_time * 1000).diff(moment()))

    if (duration > 0) this.runCountDown({ duration })
    if (duration <= 0) this.setState({ error: <Trans>Auction has ended</Trans> })
  }

  runCountDown = ({ duration, stop }) => {
    const _this = this
    // clear timer
    if(stop) {
      clearInterval(this.interval)
    }

    // timer
    this.interval = setInterval(function () {
      if (duration > 0) {
        duration = moment.duration(duration - 1000, 'milliseconds')
        let days = Math.floor(duration.as('days'))
        let hours = Math.floor(duration.as('hours') - days * 24)
        let minutes = Math.floor(duration.as('minutes') - (days * 24 * 60 + hours * 60))
        let seconds = Math.floor(duration.as('seconds') - (days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60))
       
        // set time
        _this.setState({
          days: days,
          hours: hours,
          minutes: minutes,
          seconds: seconds
        }) 
      
        // else show error
      } else {
        _this.setState({ error: <Trans>Auction has ended</Trans> })
      }
    }, 1000)
  }

  render() {
    const { timer, display } = this.props
    let ends = moment(timer * 1000)
    const {days, hours, minutes, seconds, error }  = this.state

    let showDays = days > 0 ? <Trans>{days}pv</Trans> : ''
    let showHours = (days > 0 || hours > 0) ? <Trans>{hours}t</Trans> : ''
    let showMinutes = (days > 0 || hours > 0 || minutes > 0) ? <Trans>{minutes}min</Trans> : ''
    let showSeconds = (days > 0 || hours > 0 || minutes > 0 || seconds > 0) ? <Trans>{seconds}sek</Trans> : '...'

    let displayCounter = error ? error : <p className="product__timer-time">{showDays} {showHours} {showMinutes} {showSeconds}</p>

    if(display) {
      return (
        <div className="product__timer-container">
          <div className="product__timer">
            <Trans><div className="product__timer-title">Aikaa jäljellä:</div></Trans>
            <div className="product__timer-time">{displayCounter}</div>
          </div>
          <div className="product__timer">
            <Trans><div className="product__timer-ending">
              {
                `Kohde sulkeutuu ${ends.format('DD.MM.YYYY')}
                , klo ${ends.format('HH:mm')}`
              }
            </div>
            </Trans>
          </div>
        </div>
      )
    } else {
      return <Trans><p>You need to be logged in</p></Trans>
    }
  }
}

export default inject('productStore')(observer(AuctionDetails))
