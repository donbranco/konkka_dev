import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Collapse } from 'reactstrap'
import { Trans } from '@lingui/macro'

import BidListRow from './BidListRow'

class BidList extends Component {
  constructor(props) {
    super(props)

		// this.socket = io(routes.NODE_ROOT_HTTPS)

    this.state = {
      pid: false,
      bidSet: false,
      bidListLoaded: false,
      display: false
    };
  }

  // * loading product data - from node else drupal
  // * controller for loading the data in the  bid list
  loadBidData = async pid => {
    const { getStartBidFromNode, getStartBidFromDrupal } = this.props.productStore
    const node = await getStartBidFromNode(pid)

    console.log('PID to update: ', pid )
    console.log('Check if it loads from drupal or node: ', node[0] );

    // set bidlist with the data from node
    if (typeof node[0] !== 'undefined') return this.setState({ bids: node })

    // set bidlist with the data from drupal
    // ! probably not needed.....
    const drupal = await getStartBidFromDrupal(pid)
    return this.setState({ bids: drupal })
  }

  render() {
    const { deleteBidFromList } = this.props.productStore
    const { currentUser } = this.props.userStore
    const { adminUID, bidList } = this.props
    const { display } = this.state

    const currentUID = currentUser.uid

    let toggle = display ? 'active' : 'hidden'
    // let title = display ? <Trans>Piilottaa tarjoukset</Trans> : <Trans>Näytä tarjokset</Trans>
    let total_bids = typeof bidList !== 'undefined' ? bidList.length : 0
    const bidlist_title = display ? <Trans><div className="bidlist__head-title">{`Piilottaa tarjoukset (${total_bids})`}</div></Trans>
    : <Trans><div className="bidlist__head-title">{`Näytä tarjokset (${total_bids})`}</div></Trans>

    return (
      <>
        <div className={`bidlist__container`}>
          <div>
            <div
              className="bidlist__head"
              onClick={() => { this.setState({ display: !display }) }}
            >
              {bidlist_title }
              {/* <div className="bidlist__head-title">{`${title} (${total_bids})`}</div> */}
              <div className={`bidlist__head-arrow ${toggle}`}></div>
            </div>
          </div>
          <Collapse isOpen={display} className={`bidlist`}>
            <BidListRow bids={bidList} deleteBid={deleteBidFromList} userID={currentUID} adminUID={adminUID}/>
          </Collapse>
        </div>
      </>
    )
  }
}

export default inject('productStore', 'userStore')(observer(BidList));
