import React, { PureComponent } from 'react'
import {
  Table,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap"
import classnames from 'classnames'
import { Trans } from '@lingui/macro'

import ProductDescriptionSpecs from "./ProductDescriptionSpecs"
import InfoTable from "./InfoTable"

export default class ProductDetails extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: '1'
    }
  }

  toggle = tab => {
    console.log('doe wat')
    if (this.state.activeTab !== tab) this.setState({ activeTab: tab })
  }

  render() {

    console.log(this.state)
    return (
      <>
        <Col className="product__description" sm="12" md="8" lg="8">
        <Nav tabs>
          <NavItem className={classnames({ active: this.state.activeTab === '1' })}>
            <NavLink onClick={() => this.toggle('1') } >
              <Trans>About</Trans>
            </NavLink>
          </NavItem>
          <NavItem className={classnames({ active: this.state.activeTab === '2' })}>
            <NavLink onClick={() => this.toggle('2') } >
              <Trans>Specs</Trans>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <ProductDescriptionSpecs description={this.props.description} />
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                <Table responsive>
                  <tbody className="product__description-specs">
                    <InfoTable table_content={this.props.table_content} />
                  </tbody>
                </Table>
              </Col>
            </Row>
          </TabPane>
          </TabContent>
        </Col>
      </>
    )
  }
}
