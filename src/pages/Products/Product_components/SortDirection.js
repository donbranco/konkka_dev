import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Collapse } from 'reactstrap';

class SortDirection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderbyCollapse: false,
    };
  }

  toggleSort = () => {
    this.setState({ orderbyCollapse: !this.state.orderbyCollapse });
  };

  generateSortDirection = () => {
    const { filterSubmission } = this.props;
    const _sort_direction = ['ASC', 'DESC'];

    return _sort_direction.map((direction, i) => {
      return (
        <div key={i} className='filter__sort-direction-wrapper'>
          <label htmlFor={direction}>{direction}</label>
          <input type='radio' id={direction} name='sort-direction' value={direction} onClick={ (e) => { filterSubmission(e); this.toggleSort(); }} />
        </div>
      );
    })
  }

  render() {
    const { orderbyCollapse } = this.state;
    const active = orderbyCollapse ? 'active' : '';
    const orderbyStyle = {
      fontSize: '14px',
      textAlign: 'right'
    };

    return (
      <>
        <div className="filter__sort">
          <label onClick={this.toggleSort} className={active} style={orderbyStyle}>Sort Direction</label>
          <Collapse isOpen={orderbyCollapse}>
            {this.generateSortDirection()}
          </Collapse>
        </div>
      </>
    );
  }
}

export default inject('productStore')(observer(SortDirection));
