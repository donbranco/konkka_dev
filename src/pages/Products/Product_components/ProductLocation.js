import React from 'react'
import { inject, observer } from 'mobx-react'
import { Trans } from '@lingui/macro'

// import DeliveryLogo from '../../../images/delivery.svg'
import LocationLogo from '../../../images/location.svg'

function ProductLocation() {
  const { locality, postal_code } = this.props.address
  let location, postal_area

  if(typeof locality !== 'undefined' && locality !== null) location = <div>{locality}</div>
  if(typeof postal_code !== 'undefined' && postal_code !== null) postal_area = <div>{postal_code}</div>

  return (
    <div className="product__location">
      <div className="col-sm-6 col-md-6 product__location-address">
        <img src={LocationLogo} alt={'logo'} className="product__location-delivery-image" />
        <Trans><p>Kohteen sijainti</p></Trans>
        {location}
        {postal_area}
        <div className="link__google-maps" >
          <Trans><a href={`https://www.google.com/maps/search/?api=1&query=${locality}+${postal_code}`} target="_blank" rel="noopener noreferrer">» Avaa kartalla</a></Trans>
        </div>
      </div>
      {/* <div className="col-sm-6 col-md-6 product__location-delivery">
        <img src={DeliveryLogo} alt={'logo'} className="product__location-delivery-image" />
        <p className="delivery-title">Toimitustapa</p>
        <ul>
          <li>Nouto 0€</li>
          <li>Matkahuolto 3600€</li>
          <li>Posti 4500€</li>
          <li>VR Cargo 12890€</li>
        </ul>
      </div> */}
    </div>
  )
}


export default inject('productStore')(observer(ProductLocation));