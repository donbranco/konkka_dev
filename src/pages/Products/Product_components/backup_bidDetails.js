

import React, { Component } from "react";
import {
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from "reactstrap";

// Custom imports
import Aux from "../../../hoc/Aux/Aux";

class BidForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: {},
      seconds: undefined
    };
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  secondsToActualTime() {
    var currentTime = new Date();
    var endingTime = new Date(this.props.endingTime * 1000);
    var countDownFrom = Math.abs(endingTime - currentTime) / 1000;
    this.setState({ seconds: countDownFrom });
    return countDownFrom;
  }

  secondsToTime(secs) {
    // Time left to ending date in seconds
    var delta = secs;

    let days = Math.floor(delta / 86400);
    delta -= days * 86400;

    let hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;

    let minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;

    let seconds = Math.floor(delta % 60);

    let obj = {
      days: days,
      h: hours,
      m: minutes,
      s: seconds
    };
    return obj;
  }

  startTimer() {
    if (this.timer === 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds
    });

    // Check if we're at zero.
    if (seconds === 0) {
      clearInterval(this.timer);
    }
  }

  componentWillMount() {
    // this.setState({seconds: this.props.endingTime})
    this.secondsToActualTime();
  }

  componentDidMount() {
    console.log();
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeftVar });
    this.startTimer();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    let bid_input = this.props.display ? <ListGroupItem /> : <p>You need to be logged in</p>;

    let time_format = `
      ${this.state.time.days}d 
      ${this.state.time.h}h 
      ${this.state.time.m}min 
      ${this.state.time.s}sec
    `;

    let show_time =
      new Date(this.props.endingTime * 1000) > new Date()
        ? time_format
        : "Sorry, auction has ended";

    return (
      <Aux>
        <ListGroupItem className="bid-current-price">
          <ListGroupItemText>
            <h2>Hinta nyt:</h2> <span>{this.props.bidValue}</span>
          </ListGroupItemText>
        </ListGroupItem>
        {bid_input}
        <ListGroupItem className="bid-time-left">
          <ListGroupItemHeading>Aikaa jäljellä:</ListGroupItemHeading>
          <ListGroupItemText>{show_time}</ListGroupItemText>
        </ListGroupItem>
      </Aux>
    );
  }
}

export default BidForm;
