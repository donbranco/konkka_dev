import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Collapse } from 'reactstrap'

// language trans
import { Trans } from '@lingui/macro'

class SortBy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderbyCollapse: false,
      _sortby: [
        {type: 'Highest price', type_name: <Trans>Highest price</Trans>, direction: 'DESC', sort:'price'},
        {type: 'Lowest price', type_name: <Trans>Lowest price</Trans>, direction: 'ASC', sort:'price'},
        {type: 'Ending last', type_name: <Trans>Ending last</Trans>, direction: 'DESC', sort:'ending time'},
        {type: 'Ending next', type_name: <Trans>Ending next</Trans>, direction: 'ASC', sort:'ending time'},
      ],
      selected_sort: '',
    }
  }

  componentDidMount = () => this.setState({selected_sort: this.state._sortby[3].type_name })

  // update toggle state
  toggleSort = () => this.setState({ orderbyCollapse: !this.state.orderbyCollapse})

  // update toggle label
  updateSortLabel = sort => this.setState({selected_sort: sort})

  // generate sort options
  generateSortBy = () => {
    const { filterSubmission } = this.props
     
    return this.state._sortby.map((sortby, i) => {
      const { type, type_name, sort, direction } = sortby
      return (
        <div key={i} className='filter__sort-by-wrapper'>
          <label htmlFor={type}>{type_name}</label>
          <input 
            type='radio' 
            id={type} 
            name='sort-by' 
            data-sort={sort} 
            data-direction={direction} 
            onClick={ (e) => { filterSubmission(e); this.toggleSort(); this.updateSortLabel(type_name) }} 
          />
        </div>
      )
    })
  }
  
  render() {
    const { orderbyCollapse, selected_sort } = this.state
    const orderbyStyle = { textAlign: 'right' }
    const active = orderbyCollapse ? 'active' : ''

    return (
      <>
        <div className="filter__sort">
          <div onClick={this.toggleSort} className="filter__sort-label">
            <label className={`filter__sort-text ${active}`} style={orderbyStyle}>
              { selected_sort }
            </label>
            <div className={`bidlist__head-arrow ${active}`}></div>
          </div>
          <Collapse isOpen={orderbyCollapse}>
            { this.generateSortBy() }
          </Collapse>
        </div>
      </>
    )
  }
}

export default inject('productStore')(observer(SortBy))
