import React from 'react'
import { Trans } from '@lingui/macro'

export default function BidFormMessage({ errorMessage }) {
  // if (errorMessage !== false) return <div className="alert alert-danger bid-form__alert"><strong>{errorMessage}</strong></div>

  // else return minimum bid increment
  const increment = '10.00'
  return (
    <div className="bid-form__alert-minimum" >
      <Trans><p>{`Inimikorotus on ${increment} €. Huuto on sitova. Hintaan ei lisätä arvonlisäveroa.`}</p></Trans>
    </div >
  )
}
