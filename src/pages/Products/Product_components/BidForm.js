import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import io from 'socket.io-client'
import { Form, FormGroup, Input, Button } from 'reactstrap'
import moment from 'moment'
import { Trans } from '@lingui/macro'

// routes
import routes from '../../../routes/routes'

// components
import BidFormMessage from './BidFormMessage'

class BidForm extends Component {
	constructor(props) {
		super(props)

		this.socket = io(routes.NODE_ROOT_HTTPS)

		this.state = {
			pid: this.props.pid,
			uid: this.props.uid,
			bid_input: false,
			successMessage: false,
			errorMessage: false,
			proxy: false,
			proxy_input: false,
			increment: 99,
			buy_input: false,
			bid_input_value: React.createRef(),
			proxy_input_value: React.createRef()
		}

		this.socket.on('BID_MESSAGE', message => {
			const { type, content } = message
			if(type === 'error') {
				this.setState({ errorMessage: content })
			} else {
				this.setState({ successMessage: content })
			}
			this.setNotifcation(type, content)
    })
	}

	// set notifcation snack
	setNotifcation = (type, message) => this.props.productStore.setNotification(type, message)

	// * handle submit request for buy, bids and proxies
	handleSubmit = (e) => {
		e.preventDefault()
		// init some basic data/state/variables
		const { uid } = this.props.userStore.currentUser
		const { highestBid, ending, UIDHighestBid } = this.props
		const { pid, increment, bid_input_value, proxy_input_value } = this.state
		const succ = 'success', err = 'error', submitType = e.currentTarget.name
		let currentTargetValue

		// check which form is submited and grabs the value of that input
		if(submitType === 'proxy-submit') currentTargetValue = Math.round(parseInt(proxy_input_value.current.props.value))
		if(submitType === 'bid-submit') currentTargetValue = Math.round(parseInt(bid_input_value.current.props.value))

		// is auction still active?
		if (ending * 1000 > moment().valueOf()) {
			// check input value
			if (currentTargetValue > highestBid + increment) {
				// handling submission
				if(submitType === 'bid-submit' && uid !== UIDHighestBid) this.sendBid({ pid: pid, uid: uid, bid_amount: currentTargetValue })
				if(submitType === 'bid-submit' && uid === UIDHighestBid) this.handleNotificationMessage("warning", "You currently have the highest bid")
				if(submitType === 'proxy-submit') this.setProxyBid({ pid: pid, uid: uid, max_bid: currentTargetValue, increment: increment, current_bid: this.props.highestBid, auction_ends: this.props.ending })

			} else 	if(submitType === 'buy-submit')  {
				this.sendPurchase({ pid: pid, uid: uid, bid_amount: highestBid })
				this.handleNotificationMessage(succ, 'Purchase has been approved')
				return
			} else {
				this.handleNotificationMessage(err, 'Bid was not valid')
				return
			}
			// more error handling is at the bottom comment in case if needed
		}
	}

	// handle and send bid to node
	sendBid = data => {
		console.log('[send from react to node]: ', data)
		this.props.productStore.sendBid(data)
	}

	// handle and send proxy to node
	setProxyBid = data => {
		console.log('[PROXY]: ', data)
		this.props.productStore.addProxyBid(data)
	}
	
	// handle and send proxy to node
	sendPurchase = data => {
		console.log('[Purchase/Buy]: ', data)
		this.props.productStore.sendBid(data)
	}
	
	// handle notifications
	handleNotificationMessage = (type, message) => {
		switch (type) {
			case 'success':
				this.setNotifcation(type, message)
				this.setState({ successMessage: message})
				break;
		
			default:
				this.setNotifcation(type, message)
				this.setState({ errorMessage: message})
				break;
		}
	}

	// check if user is logged in and is allowed to place bids
	isUserLoggedIn = async () => {
		const { getUserID, getToken } = this.props.commonStore
		const uid = await getUserID()
		const token = await getToken()

		if(uid && token) return true
		return false
	}

	// handle input chnages
	handleInputChange = (e) => {
		const state = e.currentTarget.name
		const value = e.currentTarget.value
		this.setState({ [state]: value, errorMessage: false, successMessage: false })
	}

	// render component
	render() {
		// init some basic data/state/variables
		const uid = this.props.userStore.currentUser
		const { ending, proxy_bidding, highestBid, auctionType, purchased } = this.props
		const { bid_input, proxy_input, errorMessage, successMessage, increment } = this.state

		// TODO: check if they can be changed into const
		let endingTime = moment() < moment(ending * 1000) ? true : false
		let sucess_text = successMessage ? 'text-success' : ''
		let error_text = errorMessage ? 'error-text' : ''
		let error_input = errorMessage ? 'error-input' : ''

		const error = <p className={ error_text }>{errorMessage}</p>
		const success = <p className={ sucess_text }>{successMessage}</p>


		// diplay on site text if it is not a online aution
		// type === '3
		const on_site = (
			<Form onSubmit={e => e.preventDefault()}>
				<FormGroup className="bid-form">
					<Trans><p>This product can only be bid on site</p></Trans>
				</FormGroup>
			</Form>
		)

		// * create and button and field for placing bids, proxies and buy now, else on site bidding
		// type === '1' || '2'
		const inputButton = (button, ref, button_text) => {
			const { bid_input_value, proxy_input_value } = this.state
			const enabled = !uid || !endingTime
			let input_value 

			if(button === 'buy') {
				button_text = 'Osta nyt'
				input_value = highestBid
			}

			if(uid) {
				if(button === 'proxy') {
					if(proxy_input || proxy_input === '') input_value = proxy_input
					button_text = <Trans>Tee proxy tarjous</Trans>
					ref = proxy_input_value
				} else if(button === 'bid') {
					if(bid_input || bid_input === '') input_value = bid_input
					button_text = <Trans>Tee sitova tarjous</Trans>
					ref = bid_input_value
				} else {
					input_value = highestBid + increment
				}
			} else {
				if(button === 'proxy') return null
				return <Trans><p className="error__bid-login">You need to be logged in, in order to bid</p></Trans>
			}

			// placeholder for wether a user is logged in or not
			const bid_placeholder = uid ? `Minimum bid: ${highestBid + increment}` : "You need to be logged in"

			// return input and button
			return (
				<Form name={button + '-submit'} onSubmit={this.handleSubmit}>
					<FormGroup className="bid-form">
						<Input
							name={ `${button}_input` }
							value={ input_value }
							ref={ ref }
							className={ error_input }
							placeholder={ bid_placeholder }
							onChange={ this.handleInputChange }
							autoComplete="off"
							disabled={ enabled || button === 'buy' }
						/>
						<Button disabled={ enabled }>{button_text}</Button>
					</FormGroup>
				</Form>
			)
		}

		// variables to display input field and button based on type of auction
		let display_input, display_proxy

		if(endingTime) {
			if(auctionType === "1") display_input = inputButton('bid')
			if(auctionType === "1" && proxy_bidding) display_proxy = inputButton('proxy')
			if(auctionType === "2" && !purchased) display_input = inputButton('buy')
			if(auctionType === "2" && purchased) display_input = false
			if(auctionType === "3") display_input = on_site
		}

		// return bid form
		return (
			<>
				{/* bid input fields based on product data */}
				{ display_input }
				{ display_proxy }	

				{/* error handling when a bid is placed */}
				{errorMessage ? error : <></>}
				{successMessage ? success : <></>}

				{/* //! minimum bid increment need to be passed down */}
				{ endingTime && display_input && uid ? <BidFormMessage errorMessage={errorMessage} /> : <></> }
			</>
		)
	}
}

export default inject('productStore', 'commonStore', 'userStore')(observer(BidForm))