import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Trans } from '@lingui/macro'

const ProductDescription = ({ description }) => {

  if (description !== null) {
    return (
      <>
        <Trans><h3>Tietoja tuotteesta</h3></Trans>
        <div className="product__description-text">{ ReactHtmlParser(description.value) }</div>
      </>
    )
  } else {
    return (
      <>
        <Trans><h3>Tietoja tuotteesta</h3></Trans>
        <Trans><p>"No product description"</p></Trans>
      </>
    )
  } 
}

export default ProductDescription