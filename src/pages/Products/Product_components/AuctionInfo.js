import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Col } from 'reactstrap'
import { Trans } from '@lingui/macro'

import AuctionDetails from './AuctionDetails'
import BidForm from './BidForm'
import BidList from './BidList'
import ProductLocation from './ProductLocation'


class AuctionInfo extends Component {
 
  render() {
    // const { highest, bids } = this.state
    const { uid, pid, adminUID, bids, time, address, error, highestBid, proxy_bidding, type, purchased, UIDHighestBid } = this.props
    const { commerce_price, starting_price } = highestBid
    const drupal_highest = commerce_price ? commerce_price : starting_price
    
    const current_ts = new Date().valueOf() / 1000
    const titleMargin = time < current_ts ? { marginBottom: 0 } : null

    let currentHighestBid = highestBid > drupal_highest ? highestBid : drupal_highest
    let price = parseFloat(currentHighestBid).toFixed(2).toString().replace(".", ",")
    let show = uid !== null ? true : false

    return (
      <>
        <Col sm="12" lg="4">
          <div className="product__price" style={titleMargin}>
            <Trans><h2>Hinta nyt</h2></Trans>
            <h2 className="current-price">{price}</h2>
          </div>
          <div className="product__price-line"></div>
          <BidForm pid={pid} uid={uid} UIDHighestBid={UIDHighestBid} highestBid={currentHighestBid} proxy_bidding={proxy_bidding} ending={time} auctionType={type} purchased={purchased} />
          <AuctionDetails display={show} timer={time} />
          <BidList pid={pid} adminUID={adminUID} bidList={bids} error={error} />
          <ProductLocation address={address} />
        </Col>
      </>
    )
  }
}


export default inject('productStore')(observer(AuctionInfo));