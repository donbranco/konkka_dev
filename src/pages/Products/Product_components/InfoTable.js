import React from 'react'

const InfoTable = ({ table_content }) => {
  if(table_content) {
    let background = true;
    return table_content.map((data, i) => {
      background = !background ? 'active' : ''
      return (
        <tr key={i} className={`product__table-tr ${background}`}>
          <th className="product__table-th" scope="row">{data.first}</th>
          <td className="product__table-td">{data.second}</td>
        </tr>
      )
    })
  }
  return <tr><td>No specs available</td></tr>
}

export default InfoTable;