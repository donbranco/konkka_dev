import React from 'react';

export default function BidListRow({ bids, deleteBid, userID, adminUID }) {

	// ! admin format will change in the future in an array width object ids
	if (typeof bids !== 'undefined' && bids.length > 0) {
		// const admin = Object.values(adminUID)[0]

		const _bid_list = bids.map((bid, i) => {
			// timestamp <<--- destruct next line
			const { uid, bid_amount, proxy } = bid.bids;
			if (typeof bid !== 'undefined') {
				let remove_button = <div className="bidlist__row-bid remove"></div>
				// if(userID === admin) remove_button = <button className="btn btn-danger" onClick={() => deleteBid(bid.pid, timestamp)}>Delete</button>
				return ( 
					<div className="bidlist__row" key={i}>
						<div className="bidlist__row-index">{i + 1}.</div>
						<div className="bidlist__row-name">{uid}</div>
						<div className="bidlist__row-proxy">{proxy ? 'Automated bid' : ''}</div>
						<div className="bidlist__row-bid amount">{bid_amount}€</div>
						{remove_button}
					</div>
				);
			} else {
				return 'error with bid rows'
			}
		});
		return _bid_list
	} else {
		return 	<div className="bidlist__row-index" style={{marginTop: 20, marginBottom:20 }}>No bids are placed</div>
	}
}
