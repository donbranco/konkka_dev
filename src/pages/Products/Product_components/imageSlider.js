import React, { Component } from 'react'
import Slider from 'react-slick'
import { Col, Modal, ModalBody, } from 'reactstrap'

// import ModalImage from ''

export default class imageSlider extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      image_url: null
    };
  }

  componentDidMount = () => {
    const modalShadow = document.getElementsByClassName("fade")
    console.log('shade:', modalShadow.length)
    if( modalShadow.length ) {
      modalShadow.addEventListener('click', () => {
        this.toggle() 
      })
    }
    // document.getElementsByClassName("fade").addEventListener('click', () => {
    //   this.toggle() 
    // })
  }

  toggle = () => {
    this.setState(prevState => ({ modal: !prevState.modal }))
  }

  setImageState = (image) => {
    console.log('set image: ', image)
    console.log('current image: ', this.state.image_url)
    if(image !== this.state.image) this.setState({ image_url: image })
  }

  // build all the images if there is more than 1 image
  BuildSlider = (images) => {
    return images.map((image, index) => {
      const image_url = image.large_url ? image.large_url : image.medium_url
      const image_background = { backgroundImage: `url(${image_url})` }

      return (
        <div key={index} className="slick__image-container">
          <div
            className="slick__image-content"
            alt={image.title}
            style={image_background}
            onClick={ () => {
              this.toggle();
              this.setImageState(image_url)
            }}
          />
        </div>
      )
    })
  }  

  // build only the first image
  SetImage = (image) => {
    return (
      <div 
        className="product__header"
        onClick={ () => {
          this.toggle(); 
          this.setImageState(image.large_url)
        }} >
        <img src={image.large_url} alt={image.title} />
      </div>
    )
  }


  render() {
    const images = this.props.images
    // slider settings && image thumbnails
    const settings = {
      customPaging: function(i) {
        return (
          <a>
            <img
              src={images[i].thumbnail_url}
              alt={images[i].title}
            />
          </a>
        );
      },
      dots: true,
      dotsClass: 'slick-dots slick-thumb',
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    }


    return (
      <>
        <Col sm="12" lg="8">
          <Slider {...settings}>
            {images.length > 0 ? this.BuildSlider(images) : this.SetImage(images)}
          </Slider>
          <ModalImage toggle={this.toggle} modal={this.state.modal} image={this.state.image_url} />
        </Col >
      </>
    )
  }
}



// modal image component
function ModalImage({ toggle, modal, image }) {
  if(modal) {
    return ( 
      <Modal onClick={toggle} isOpen={modal}  isSelected={modal} className="image-modal">
        <ModalBody style={{ backgroundImage: `url(${image})`}} className="slider-modal"></ModalBody>
      </Modal>
    )
  }
  return <></>
}
