import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Container, Row, Col } from 'reactstrap';
import { Trans } from '@lingui/macro'

import Loader from '../../../components/UI/Loader/Loader';
import Favourites from './Favourites';
import ListOfPlacedBids from './ListOfPlacedBids';

class MyItems extends Component {
	componentDidMount = async () => {
		const auctions_with_placed_bids = await this.props.profileStore.getAllProducts();
		// const auctions_favourites = await this.props.profileStore.getFavourites();

		if (auctions_with_placed_bids) {
			this.props.profileStore.setActiveAuctionResults(auctions_with_placed_bids);
		}

		// if (auctions_favourites) {
		// 	this.props.profileStore.setFavouriteAuctionResults(auctions_with_placed_bids);
		// }
	};

	render() {
		const { active_auctions, favourites_auctions, placed_bids, product_data } = this.props.profileStore;
		const { currentUser } = this.props.userStore

		if (active_auctions && favourites_auctions && currentUser) {
			return (
				<Container fluid>
					<Row>
						<Col>
							<Trans><h1>Omat huudot</h1></Trans>
						</Col>
					</Row>
					<Row>
						<Col sm="12" md="12" lg="12">
							<Trans><h2>list of placed bids</h2></Trans>
						</Col>
						<Col sm="12" md={{ size: 9 }} lg={{ size: 9 }} className="omat-huudot">
							<ListOfPlacedBids
								user={currentUser}
								auctions={active_auctions}
								placed_bids={placed_bids}
								product_data={product_data}
							/>
						</Col>
					</Row>
					<Row>
						<Col sm="12" md="12" lg="12">
							<Trans><h2>Favourite Auctions</h2></Trans>
						</Col>
						<Col sm="12" md={{ size: 9 }} lg={{ size: 9 }} className="omat-huudot">
							<Favourites auctions={favourites_auctions} />
						</Col>
					</Row>
				</Container>
			);
		} return <Loader />
	}
}

export default inject('profileStore', 'userStore')(observer(MyItems));
