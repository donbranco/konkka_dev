import React from 'react'
import { Table } from 'reactstrap'
import moment from 'moment'
import { Trans } from '@lingui/macro'

export default function ListOfPlacedBids(props) {
	const { placed_bids, auctions, product_data } = props
	// const { placed_bids, auctions, product_data, user } = props

	console.log('webwkefuw: ', auctions)
	console.log('234234324: ', placed_bids)

	return (
		<Table responsive hover className="omat-huudot__table">
		<thead>
			<tr>
				<th>#</th>
				<Trans><th>Auction</th></Trans>
				<Trans><th>Highest bid</th></Trans>
				<Trans><th>Placed bid</th></Trans>
				<Trans><th>Ending Time</th></Trans>
			</tr>
		</thead>
		<tbody>
			{auctions ? 
				<GenerateTable placed_bids={placed_bids} auction_data={auctions} product_data={product_data} />
				: <NoTableContent />
				}
		</tbody>
		</Table>
	)
	
}

function GenerateTable(props) {
	// product_data
	const { placed_bids, auction_data } = props

	// helper function to get highest bid
	function getHighestBid(auction) {
		const highest_bid_index = auction.bids.length - 1
		return auction.bids[highest_bid_index].bid_amount
	}

	// helper function to get product/auction name
	function getProductName(pid) {
		const product = JSON.parse(props.product_data.data.products[pid])
		return product.title
	}

	function getProductName2(e) {
		console.log(e)
		// const product = JSON.parse(props.product_data.data.products[pid])
		// return product.title
	}

	const _bid_rows = placed_bids.map((bid, i) => {
		// TODO: get product meta data such as title from drupal API
		const auction = auction_data[i]
		const auction_ending = moment(parseInt(auction.auction_ends) * 1000).format('MMMM Do YYYY, h:mm:ss a')

		const product_name = getProductName(auction.pid)
		const highest_bid = getHighestBid(auction)
		const user_bid = bid.bid_amount

		return (
			<tr
				key={i}
				id={auction.pid}
				class="omat-huudot__row"
				onClick={(e) => window.location = window.location.origin + '/product/' + e.currentTarget.id }
				// onClick={getProductName2}
			>
				<th scope="row">{i}</th>
				<td>{product_name}</td>
				<td>{highest_bid}</td>
				<td>{user_bid}</td>
				<td>{auction_ending}</td>
			</tr>
		)
	})
	return _bid_rows
} // end generate table component

function NoTableContent() {
	return (
		<tr>
			<p>No bids were placed yet</p>
		</tr>
	)
} // end no content table
