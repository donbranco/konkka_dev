import React, { Component } from 'react'
import { Table } from 'reactstrap'
import { Trans } from '@lingui/macro'

export default class ListOfPlacedBids extends Component {
	GenerateTable = () => {
		return (
			<tr>
				{/* // TODO: create a map function to generate table */}
				<th scope="row">1</th>
				<td>BMW</td>
				<td>100.000</td>
				<td>85.99</td>
				<td>1st of May</td>
				{/* // TODO: a href="/product/:id" */}
				<td>GO</td>
			</tr>
		);
	};

	NoTableContent = () => {
		return (
			<tr>
				<td>No auctions are being followed</td>
			</tr>
		);
	};

	render() {
		return (
			<Table responsive hover className="omat-huudot__table">
				<thead>
					<tr>
						<th>#</th>
						<Trans><th>Auction</th></Trans>
						<Trans><th>Highest bid</th></Trans>
						<Trans><th>Placed bid</th></Trans>
						<Trans><th>Ending Time</th></Trans>
						<Trans><th>Go to auction</th></Trans>
					</tr>
				</thead>
				<tbody>{true ? this.GenerateTable() : this.NoTableContent()}</tbody>
			</Table>
		);
	}
}
