module.exports = {
	// Dev server Routes
	// NODE_ROOT_HTTPS: 'http://localhost:8000',
	NODE_ROOT_HTTPS: 'https://dev.konkurssi.donbran.co:8000',
	DRUPAL_ROOT_HTTPS: 'https://dev2.konkurssi.donbran.co',
	
	API_ROOT: 'https://dev2.konkurssi.donbran.co/RESTful',
	DRUPAL_ROOT: 'https://dev2.konkurssi.donbran.co',
	DRUPAL_API_ROOT: 'https://dev2.konkurssi.donbran.co/RESTful',
};
