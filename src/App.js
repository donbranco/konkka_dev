import React, { Component } from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
// import { autorun } from 'mobx'
import { inject, observer } from 'mobx-react'


// language settings
import { I18nProvider } from '@lingui/react'

// style components
import Loader from './components/UI/Loader/Loader'
import Notification from './components/snackbar/snackbar'

// Pages
import Frontpage from './pages/Frontpage/Frontpage'
import Product from './pages/Products/SingleProductPage'
import Login from './pages/Auth/Login/Login'
import StaticPage from './pages/Static/Header/page'
// import Profile from './pages/Profile/Details/Details'
import MyItems from './pages/Profile/MyItems/Huudot'
import Contact from './pages/Contact/Contact'
import Footer from './components/Footer/Footer'
import Log from './pages/Admin/Logs'

// Components
import Header from './components/Header/Header'
import StaticPages from './components/Static/StaticHeader'
import api from './api';


class App extends Component {
  
  componentWillMount() {
    const { setAppLoaded } = this.props.commonStore
    const { getProductDetailList } = this.props.productStore
    const { user_auth_cookie } = this.props.authStore
    const { pullUser } = this.props.userStore

    // track device screen size
    this.props.layoutStore.trackScreenWidth()

    console.log('App is loading.... ')
    console.log('App user tokens/cookies .... ', user_auth_cookie)

    if (typeof user_auth_cookie.value !== 'undefined' || user_auth_cookie.value !== '') {
      console.log('TOKENS && COOKIE EXIST PULLING USER DATA...')

      // run all function/api calls synchronously
      const promise2 = getProductDetailList()
      const promise3 = pullUser()

      Promise.all([ promise2, promise3])
      .then(() => setAppLoaded())
      .catch(e => console.log('something went wrong while init: ', e))
    } else {
      console.log('LOADING WITHOUT PULLING USER...')
      window.localStorage.removeItem('user_auth_token')
      // load default page when there is no user cookie
      // run all function/api calls synchronously
      const promise2 = getProductDetailList()

      Promise.all([ promise2 ])
      .then(() => setAppLoaded())
      .catch(e => console.log('something went wrong while init: ', e))
    }
    // get static pages
    this.loadStaticPages()
  }

  loadStaticPages = () => {
    api.Pages.getStaticPagesHeader()
    .then(res => this.props.layoutStore.setStaticPages(res.data.pages))
    .catch(e => console.log('static pages failed: ', e))
  }
    
  render() {
    const { notificationAlert, notificationType, notificationMessage, clearNotification } = this.props.productStore
    const { staticPages } = this.props.layoutStore
    const { languageSetting, catalog } = this.props.commonStore
    const { uid } = this.props.userStore.currentUser

    // const catalogs = { nl: catalogCs };

    if (this.props.commonStore.appLoaded) {
      return (
        <>
        <I18nProvider language={languageSetting} catalogs={catalog} >
            <div className="App">
              <StaticPages uid={uid} pages={staticPages}/>
              <Header />
              <Switch>
                <Route exact path="/" component={Frontpage} />
                <Route exact path="/page/:id" component={StaticPage} />
                <Route exact path="/yhteystiedot" component={Contact} />
                <Route exact path="/kirjaudu" component={Login} />
                <Route exact path="/profiili/details" component={MyItems} />
                <Route exact path="/profiili/omat-huudot" component={MyItems} />
                <Route exact path="/product/:id" component={Product} />
                <Route exact path="/admin/log" component={Log} />
              </Switch>
            
              <Notification alert={notificationAlert} type={notificationType} message={notificationMessage} clear={clearNotification} />
            </div>
            <Footer />
          </I18nProvider>
         </>
      );
    } else {
      return (
        <div className="App">
          <Loader />
        </div>
      );
    }
  }
}

export default inject('userStore', 'authStore', 'commonStore', 'productStore', 'layoutStore')(withRouter(observer(App)));
