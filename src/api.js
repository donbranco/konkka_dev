import axios from 'axios'
import commonStore from './stores/commonStore'

import routes from './routes/routes'

const NODE_ROOT = routes.NODE_ROOT_HTTPS
const DRUPAL_API_ROOT = routes.DRUPAL_API_ROOT

// header -- sends tokens when user is logged in
const getHeader = async () => {	

  const token = await commonStore.getToken()
  const sessionID = await commonStore.getSessionID()

	if(token === null || sessionID === null) {
		return { 
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",
			}
		}
	} else {
		return { 
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",

				"X-USER-SESSION-TOKEN": sessionID,
				"X-CSRF-TOKEN": token,
			}
		}
	}
}

// Request typesget user data with tokens:
const requests = {
  get: async (root, url, body) => {
    return await axios.get(`${root}${url}`, body)
  },
  post: async (root, url, body) => {
    return await axios({
      url: `${root}${url}`,
      method: 'POST',
      data: body,
      withCredentials: true
    })
  },
  delete: async (root, url, body) => {
    const header = await getHeader()
    return await axios.delete(`${root}${url}`, header, body)
  }
}

const Auth = {
  current: async () => {
    const cookie = await JSON.parse(commonStore.getHeaderToken())
    const token = cookie.token
    const session_id = cookie.sessionid
    const uid = cookie.uid

    console.log(uid)
    
    return await axios({
      url: DRUPAL_API_ROOT + '/user/' + uid,
      method: 'GET',
      headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",

				"X-USER-SESSION-TOKEN": session_id,
				"X-CSRF-TOKEN": token,
			},
      withCredentials: true
    })
    .catch(e => console.error(e))
  },
  login: async (user) => {
    return await axios({
      url: DRUPAL_API_ROOT + '/user/login',
      method: 'POST',
      data: {
        username: user.username,
        password: user.password
      },
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      withCredentials: true
    })
  },
  logout: async () => {
    let cookie = await commonStore.getHeaderToken()

    try {
      cookie = JSON.parse(cookie)
    } catch(e) {
      return cookie
    }

    const token = cookie.token
    const session_id = cookie.sessionid

    axios.post(`${routes.DRUPAL_API_ROOT}/user/logout`, null, {
      headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',

      "X-USER-SESSION-TOKEN": session_id,
      "X-CSRF-TOKEN": token,
      },
      withCredentials: true
    })
  }
}

const Products = {
  productDetails: async (body) => {
    const res = await requests.post(DRUPAL_API_ROOT, `/products/get`, body)
    console.log('[ response getting products ] ---- ', res)
    return res
  }
}

const Filter = {
  getByAdvandedFilter: async (body) => {
    return await requests.post(DRUPAL_API_ROOT, `/products/get`, body)
  }
}

const Tags = {
  listTags: async () => {
    return await requests.get(DRUPAL_API_ROOT, `/tags/get`)
  },
  locations: async () => {
    return await axios.get(DRUPAL_API_ROOT + '/locations/get')
  }
}

const Bids = {
  listBids: async (pid) => {
    return await requests.get(NODE_ROOT, `/productbids/` + pid)
  },
  listBidsByHighest: async (pid) => {
    return await requests.get(NODE_ROOT, `/productbids/` + pid + `/sort/highest`)
  },
  deleteBid: async ({ pid, body, params }) => {
    return await axios.post(`${NODE_ROOT}/productbids/remove/bid/${pid}`, body, params)
  }
}

// TODO: user data request such as: favourite auctions §§ auctions with placed bids.
// ? Get user data from Drupal or search on Node based on user_id
const Profile = {
  getAllProducts: async () => {
    return await requests.get(NODE_ROOT, `/productbids`)
  },
  getPlacedBids: async (pid) => {
    return await requests.get(NODE_ROOT, `/productbids/` + pid)
  },
  favouriteAuctions: async (pid) => {
    // const new_pid = parseInt(pid)
    return await requests.get(NODE_ROOT, `/productbids/` + pid + `/sort/highest`)
  },
  removeFavouriteAuction: async ({ pid, body, params }) => {
    return await axios.post(`${NODE_ROOT}/productbids/remove/bid/${pid}`, body, params)
  }
}

const Pages = {
  getStaticPagesHeader: async () => {
    return await axios.get(`${DRUPAL_API_ROOT}/pages/get`)
  },
}

const Log = {
  getAllLogs: async () => {
    return await axios.get(`${NODE_ROOT}/logs/`)
  },
  getLogByType: async (type) => {
    console.log(`${NODE_ROOT}/logs/type/${type}`)
    return await axios.get(`${NODE_ROOT}/logs/type/${type}`)
  },
  postNewLog: async (body) => {
    console.log('posting new log: ', body)
    return await axios.post(`${NODE_ROOT}/logs/add`, body)
  },
}

export default {
  Auth,
  Bids,
  Filter,
  Tags,
  Products,
  Profile,
  Pages,
  Log
}
